package fr.axileo.axileoapplication.launchers

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.annotation.NonNull
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import fr.axileo.axileoapplication.R
import fr.axileo.axileoapplication.events.CreateCraEvent
import fr.axileo.axileoapplication.events.UpdateToolbarTitleEvent
import fr.axileo.axileoapplication.events.UserLoggedEvent
import fr.axileo.axileoapplication.features.clients.ui.fragments.ClientsFragment
import fr.axileo.axileoapplication.features.contacts.ui.fragments.ContactFragment
import fr.axileo.axileoapplication.features.cra.ui.fragments.CraFragment
import fr.axileo.axileoapplication.features.cra.ui.fragments.CraListFragment
import fr.axileo.axileoapplication.features.events.entities.Event
import fr.axileo.axileoapplication.features.events.ui.fragments.EventsFragment
import fr.axileo.axileoapplication.features.jobs.ui.fragments.JobsFragment
import fr.axileo.axileoapplication.features.login.entities.Account
import fr.axileo.axileoapplication.features.login.ui.LoginFragment
import fr.axileo.axileoapplication.features.members.ui.fragments.MembersFragment
import fr.axileo.axileoapplication.features.presentation.ui.fragments.PresentationFragment
import fr.axileo.axileoapplication.utils.FragmentsUtil
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import javax.inject.Inject

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.nav_view) lateinit var navigationView: NavigationView
    @BindView(R.id.drawer_layout) lateinit var drawerLayout: DrawerLayout
    @BindView(R.id.toolbar) lateinit var toolbar: Toolbar
    @BindView(R.id.fragments_container) lateinit var fragmentsContainer: FrameLayout
    lateinit var headerLayout: LinearLayout
    lateinit var connectionStateTxt: TextView
    lateinit var connectionIntranetTxt: TextView
    lateinit var userPicture: ImageView


    @Inject lateinit var eventBus: EventBus

    lateinit var events: ArrayList<Event>
    var isUserLogged: Boolean? = false
    var account: Account? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AxileoApplication.getApplication(this).getActivitiesComponent().resolveDependencies(this)
        setContentView(R.layout.activity_main)
        ButterKnife.bind(this)
        setSupportActionBar(toolbar)
        val toggle = ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
        navigationView.setNavigationItemSelectedListener(this)
        headerLayout =  navigationView.getHeaderView(0) as LinearLayout
        connectionStateTxt = headerLayout.findViewById(R.id.connection_state)
        connectionIntranetTxt = headerLayout.findViewById(R.id.connection_intranet)
        userPicture = headerLayout.findViewById(R.id.imageView)

        if (intent != null) {
             events = intent.getParcelableArrayListExtra(EVENTS_BUNDLE)
            isUserLogged = intent.getBooleanExtra(LOGGED_USER_BUNDLE, false)
            account = intent.getParcelableExtra(ACCOUNT_BUNDLE)
        } else {
            isUserLogged = savedInstanceState?.getBoolean(LOGGED_USER_BUNDLE, false)
            account = savedInstanceState?.getParcelable(ACCOUNT_BUNDLE)
        }

        headerLayout.setOnClickListener({
            drawerLayout.closeDrawer(GravityCompat.START)
            FragmentsUtil.setFragment(this, LoginFragment.newInstance(account), LoginFragment.TAG,
                    fragmentsContainer.id, true)
        })

        if (account != null) {
            showAccountInfo()

        }

        if (savedInstanceState == null) {
            supportActionBar?.title = getString(R.string.events)
            val eventsFrag = EventsFragment.newInstance(events)
            FragmentsUtil.setFragment(this, eventsFrag, EventsFragment.TAG, fragmentsContainer.id,
                    true)
        }
    }

    override fun onResume() {
        super.onResume()
        if (!eventBus.isRegistered(this)) eventBus.register(this)
    }

    override fun onStop() {
        super.onStop()
        if (eventBus.isRegistered(this)) eventBus.unregister(this)
    }

    override fun onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            if (supportFragmentManager.backStackEntryCount == 1) {
                finish()
            } else {
                super.onBackPressed()
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        isUserLogged?.let { outState?.putBoolean(LOGGED_USER_BUNDLE, it) }
        outState?.putParcelable(ACCOUNT_BUNDLE, account)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        var fragment: Fragment? = null
        var tag = ""
        when (item.itemId) {
            R.id.presentation -> {
                supportActionBar?.title = getString(R.string.presentation)
                fragment = PresentationFragment.newInstance()
                tag = PresentationFragment.TAG
            }
            R.id.events -> {
                supportActionBar?.title = getString(R.string.events)
                fragment = EventsFragment.newInstance(events)
                tag = EventsFragment.TAG
            }
            R.id.members -> {
                supportActionBar?.title = getString(R.string.members)
                fragment = MembersFragment.newInstance()
                tag = MembersFragment.TAG
            }
            R.id.references -> {
                supportActionBar?.title = getString(R.string.clients)
                fragment = ClientsFragment.newInstance()
                tag = ClientsFragment.TAG
            }
            R.id.offers -> {
                supportActionBar?.title = getString(R.string.offers)
                fragment = JobsFragment.newInstance()
                tag = JobsFragment.TAG
            }
            R.id.contact -> {
                supportActionBar?.title = getString(R.string.contact)
                fragment = ContactFragment.newInstance()
                tag = ContactFragment.TAG
            }
            R.id.cra -> {
                supportActionBar?.title = getString(R.string.cra)
                fragment = CraListFragment.newInstance()
                tag = CraListFragment.TAG
            }
        }
        drawerLayout.closeDrawer(GravityCompat.START)

        if (fragment != null) {
            FragmentsUtil.setFragment(this, fragment, tag, fragmentsContainer.id, true)
        }
        return true
    }

    @SuppressWarnings("unused")
    @Subscribe
    fun updateToolbarTitle(event: UpdateToolbarTitleEvent) {
        supportActionBar?.title = event.title
    }

    @SuppressWarnings("unused")
    @Subscribe
    fun populateUserInfo(event: UserLoggedEvent) {
        account = event.account
        showAccountInfo()
    }

    @SuppressWarnings("unused")
    @Subscribe
    fun launchCraCreation(event: CreateCraEvent) {
        FragmentsUtil.setFragment(this, CraFragment.newInstance(), CraFragment.TAG, fragmentsContainer.id, true)
    }

    private fun showAccountInfo() {
        navigationView.menu.findItem(R.id.cra).isVisible = true
        navigationView.menu.findItem(R.id.absence).isVisible = true
        connectionStateTxt.text = "${account?.accountInfo?.lastName} ${account?.accountInfo?.name}"
        connectionIntranetTxt.visibility = View.GONE
        userPicture.setImageResource(R.drawable.axileo_icon)
    }

    companion object {
        private val TAG = MainActivity::class.java.simpleName
        private val EVENTS_BUNDLE = TAG + ":EVENTS_BUNDLE"
        private val LOGGED_USER_BUNDLE = TAG + ":LOGGED_USER_BUNDLE"
        private val ACCOUNT_BUNDLE = TAG + ":ACCOUNT_BUNDLE"

        fun newIntent(@NonNull context: Context, @NonNull events: ArrayList<Event>, isLogged: Boolean,
                      account: Account?) : Intent {
            val intent = Intent(context, MainActivity::class.java)
            intent.putParcelableArrayListExtra(EVENTS_BUNDLE, events)
            intent.putExtra(LOGGED_USER_BUNDLE, isLogged)
            intent.putExtra(ACCOUNT_BUNDLE, account)
            return intent
        }
    }
}
