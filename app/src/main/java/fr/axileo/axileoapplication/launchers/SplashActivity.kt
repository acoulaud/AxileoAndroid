package fr.axileo.axileoapplication.launchers

import android.content.SharedPreferences
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import fr.axileo.axileoapplication.R
import fr.axileo.axileoapplication.constants.SharedPreferencesConstants.Companion.TOKEN_SHARED_PREFERENCE
import fr.axileo.axileoapplication.constants.SharedPreferencesConstants.Companion.USER_LOGIN
import fr.axileo.axileoapplication.constants.SharedPreferencesConstants.Companion.USER_PASSWORD
import fr.axileo.axileoapplication.features.authentication.constants.AuthenticationConstants.Companion.HEADER_BEARER
import fr.axileo.axileoapplication.features.authentication.entities.OauthToken
import fr.axileo.axileoapplication.features.authentication.presenters.AuthenticateContract
import fr.axileo.axileoapplication.features.authentication.presenters.AuthenticatePresenter
import fr.axileo.axileoapplication.features.events.entities.Event
import fr.axileo.axileoapplication.features.events.presenters.EventsContract
import fr.axileo.axileoapplication.features.events.presenters.EventsPresenter
import fr.axileo.axileoapplication.features.login.entities.Account
import fr.axileo.axileoapplication.features.login.presenters.LoginContract
import fr.axileo.axileoapplication.features.login.presenters.LoginPresenter
import javax.inject.Inject

/**
 * Created by BenChaabane on 16/01/2018.
 */

class SplashActivity : AppCompatActivity(), EventsContract.View, AuthenticateContract.View, LoginContract.View {
    @Inject lateinit var sharedPreferences: SharedPreferences

    lateinit var eventsPresenter: EventsPresenter
    lateinit var authenticatePresenter: AuthenticatePresenter
    lateinit var loginPresenter: LoginPresenter
    lateinit var events: ArrayList<Event>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AxileoApplication.getApplication(this).getActivitiesComponent().resolveDependencies(this)
        setContentView(R.layout.activity_splash)
        events = savedInstanceState?.getParcelableArrayList(EVENTS_BUNDLE) ?: ArrayList()
        eventsPresenter = EventsPresenter(this)
        authenticatePresenter = AuthenticatePresenter(this)
        loginPresenter = LoginPresenter(this)
    }

    override fun onResume() {
        super.onResume()
        eventsPresenter.attachPresenter(this, lifecycle)
        authenticatePresenter.attachPresenter(this, lifecycle)
        loginPresenter.attachPresenter(this, lifecycle)
        if (events.size == 0) eventsPresenter.getEvents() else authenticatePresenter.authenticate()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putParcelableArrayList(EVENTS_BUNDLE, events)
    }

    override fun onEventsRetrieved(events: ArrayList<Event>) {
        this.events = events
        authenticatePresenter.authenticate()
    }

    override fun onEventsError(throwable: Throwable) {
        AlertDialog.Builder(this)
                .setMessage(R.string.splash_warning_text)
                .setOnDismissListener({ finish() })
                .create()
                .show()
    }

    override fun onAuthenticationSuccess(oauthToken: OauthToken) {
        sharedPreferences.edit().putString(TOKEN_SHARED_PREFERENCE, oauthToken.accessToken).apply()
        Log.d(TAG, "Token : " + oauthToken.accessToken)
        val userPassword = sharedPreferences.getString(USER_PASSWORD, "")
        val userLogin = sharedPreferences.getString(USER_LOGIN, "")
        Log.d(TAG, "user : $userLogin:$userPassword")
        if (userPassword.isEmpty()) {
            goToMain(false, null)
        } else {
            loginPresenter.login(userLogin, userPassword, "$HEADER_BEARER${oauthToken.accessToken}")
        }
    }

    override fun onAuthenticationFailed(throwable: Throwable) {
        Log.e(TAG, throwable.message, throwable)
        goToMain(false, null)
    }

    override fun onUserLogged(account: Account) {
        goToMain(true, account)
    }

    override fun onTokenInvalid() {
        Log.e(TAG, "Token invalid, trying to get new one")
        authenticatePresenter.authenticate()
    }

    override fun onLoginFailed() {
        AlertDialog.Builder(this)
                .setMessage(R.string.splash_login_warning_text)
                .setPositiveButton(android.R.string.ok) { dialog, which -> goToMain(false, null)}
                .create()
                .show()
    }

    override fun onPasswordChangeRequestSucceded() {
        // Nothing to do here
    }

    override fun onPasswordChangeRequestFailed() {
        // Nothing to do here
    }

    fun goToMain(isLogged: Boolean, account: Account?) {
        Thread.sleep(2500)
        val intent = MainActivity.newIntent(this, events, isLogged, account)
        startActivity(intent)
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        finish()
    }

    companion object {
        val TAG = SplashActivity::class.java.simpleName
        val EVENTS_BUNDLE = TAG + ":EVENTS_BUNDLE"
    }
}