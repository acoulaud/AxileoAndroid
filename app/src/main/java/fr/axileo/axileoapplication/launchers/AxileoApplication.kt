package fr.axileo.axileoapplication.launchers

import android.content.Context
import android.support.annotation.NonNull
import android.support.multidex.MultiDexApplication
import com.facebook.drawee.backends.pipeline.Fresco
import fr.axileo.axileoapplication.dagger.components.ApplicationComponent
import fr.axileo.axileoapplication.dagger.components.DaggerApplicationComponent
import fr.axileo.axileoapplication.dagger.modules.ApplicationModule

/**
 * Created by BenChaabane on 15/01/2018.
 */

class AxileoApplication : MultiDexApplication() {

    lateinit var applicationComponent: ApplicationComponent

    override fun onCreate() {
        super.onCreate()
        application = this
        initDaggerGraph()
        Fresco.initialize(this)
    }

    override fun onTrimMemory(level: Int) {
        super.onTrimMemory(level)
        val imagePipeline = Fresco.getImagePipeline()
        imagePipeline.clearCaches()
    }

    fun initDaggerGraph() {
        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(ApplicationModule(this))
                .build()
    }

    fun getAppComponent() = applicationComponent

    fun getPresentersComponent() = applicationComponent.getPresentersComponent()

    fun getActivitiesComponent() = applicationComponent.getActivitiesComponent()

    fun getFragmentsComponent() = applicationComponent.getFragmentsComponent()

    fun getDialogComponent() = applicationComponent.getDialogsComponent()

    companion object {
        lateinit var application: AxileoApplication
        fun getApplication(@NonNull context: Context): AxileoApplication {
            return if (application != null) application else getApplication(context)
        }
    }
}