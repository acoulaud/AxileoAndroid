package fr.axileo.axileoapplication.events

import fr.axileo.axileoapplication.features.login.entities.Account

/**
 * Created by BenChaabane on 26/01/2018.
 */
class UserLoggedEvent(var account: Account) {

    override fun toString(): String {
        return "UserLoggedEvent(account=$account)"
    }
}