package fr.axileo.axileoapplication.events

import fr.axileo.axileoapplication.features.cra.entities.CraDay

/**
 * Created by BenChaabane on 29/01/2018.
 */
class AbsenceMotifEvent(var craDay: CraDay) {
    override fun toString(): String {
        return "AbsenceMotifEvent(craDay=$craDay)"
    }
}