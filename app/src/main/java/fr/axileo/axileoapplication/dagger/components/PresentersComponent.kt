package fr.axileo.axileoapplication.dagger.components

import dagger.Subcomponent
import fr.axileo.axileoapplication.features.authentication.presenters.AuthenticatePresenter
import fr.axileo.axileoapplication.features.clients.presenters.ClientsPresenter
import fr.axileo.axileoapplication.features.cra.presenters.CraListPresenter
import fr.axileo.axileoapplication.features.cra.presenters.CraPresenter
import fr.axileo.axileoapplication.features.events.presenters.EventsPresenter
import fr.axileo.axileoapplication.features.jobs.presenters.JobsPresenter
import fr.axileo.axileoapplication.features.login.presenters.LoginPresenter
import fr.axileo.axileoapplication.features.members.presenters.MembersPresenter
import javax.inject.Singleton

/**
 * Created by BenChaabane on 15/01/2018.
 */

@Singleton
@Subcomponent
interface PresentersComponent {
    fun resolveDependencies(eventsPresenter: EventsPresenter)
    fun resolveDependencies(membersPresenter: MembersPresenter)
    fun resolveDependencies(clientsPresenter: ClientsPresenter)
    fun resolveDependencies(jobsPresenter: JobsPresenter)
    fun resolveDependencies(authenticatePresenter: AuthenticatePresenter)
    fun resolveDependencies(loginPresenter: LoginPresenter)
    fun resolveDependencies(craPresenter: CraPresenter)
    fun resolveDependencies(craListPresenter: CraListPresenter)
}