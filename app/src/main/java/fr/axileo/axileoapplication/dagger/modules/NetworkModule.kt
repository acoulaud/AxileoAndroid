package fr.axileo.axileoapplication.dagger.modules

import android.content.Context
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import io.reactivex.annotations.NonNull
import io.reactivex.schedulers.Schedulers
import okhttp3.Cache
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Field
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

/**
 * Created by BenChaabane on 15/01/2018.
 */

@Module
class NetworkModule {

    @Singleton
    @Provides
    fun provideOkhttpClient(@NonNull context: Context): OkHttpClient {
        val okHttpClient = OkHttpClient.Builder()
        val cache = Cache(context.cacheDir, CACHE_SIZE)
        okHttpClient.cache(cache)
        okHttpClient.readTimeout(1, TimeUnit.MINUTES)
        okHttpClient.connectTimeout(1, TimeUnit.MINUTES)
        return okHttpClient.build()
    }

    @Singleton
    @Provides
    fun provideRetrofit(@NonNull okHttpClient: OkHttpClient, @NonNull gson: Gson): Retrofit {
        return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
    }

    @Singleton
    @Provides
    @Named("intranet")
    fun provideIntranetRetrofit(@NonNull okHttpClient: OkHttpClient, @NonNull gson: Gson): Retrofit {
        return Retrofit.Builder()
                .baseUrl(INTRANET_BASE_URL)
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
    }

    companion object {
        private val CACHE_SIZE = 10 * 1024 * 1024L
        private val BASE_URL = "http://www.axileo.net/"
        private val INTRANET_BASE_URL = "https://intranet.mca-groupe.com/API/web/app.php/"
    }
}