package fr.axileo.axileoapplication.dagger.components

import dagger.Component
import fr.axileo.axileoapplication.dagger.modules.ApplicationModule
import fr.axileo.axileoapplication.dagger.modules.NetworkModule
import fr.axileo.axileoapplication.dagger.modules.RepositoriesModule
import fr.axileo.axileoapplication.dagger.modules.UseCasesModule
import javax.inject.Singleton


/**
 * Created by BenChaabane on 15/01/2018.
 */

@Singleton
@Component(modules = arrayOf(ApplicationModule::class, NetworkModule::class, RepositoriesModule::class,
        UseCasesModule::class))
interface ApplicationComponent {

    fun getPresentersComponent(): PresentersComponent

    fun getActivitiesComponent(): ActivitiesComponent

    fun getFragmentsComponent(): FragmentsComponent

    fun getDialogsComponent() : DialogComponents
}