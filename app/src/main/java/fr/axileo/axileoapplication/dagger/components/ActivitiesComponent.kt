package fr.axileo.axileoapplication.dagger.components

import dagger.Subcomponent
import fr.axileo.axileoapplication.launchers.MainActivity
import fr.axileo.axileoapplication.launchers.SplashActivity
import javax.inject.Singleton

/**
 * Created by BenChaabane on 23/01/2018.
 */
@Singleton
@Subcomponent
interface ActivitiesComponent {
    fun resolveDependencies(splashActivity: SplashActivity)
    fun resolveDependencies(mainActivity: MainActivity)
}