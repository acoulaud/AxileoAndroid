package fr.axileo.axileoapplication.dagger.components

import dagger.Subcomponent
import fr.axileo.axileoapplication.features.clients.ui.fragments.ClientsFragment
import fr.axileo.axileoapplication.features.contacts.ui.fragments.ContactFragment
import fr.axileo.axileoapplication.features.cra.ui.fragments.CraFragment
import fr.axileo.axileoapplication.features.cra.ui.fragments.CraListFragment
import fr.axileo.axileoapplication.features.events.ui.fragments.EventsFragment
import fr.axileo.axileoapplication.features.jobs.ui.fragments.JobsFragment
import fr.axileo.axileoapplication.features.login.ui.LoginFragment
import fr.axileo.axileoapplication.features.members.ui.fragments.MembersFragment
import fr.axileo.axileoapplication.features.presentation.ui.fragments.PresentationFragment
import javax.inject.Singleton

/**
 * Created by BenChaabane on 23/01/2018.
 */

@Singleton
@Subcomponent
interface FragmentsComponent {

    fun resolveDependencies(presentationFragment: PresentationFragment)
    fun resolveDependencies(clientsFragment: ClientsFragment)
    fun resolveDependencies(contactFragment: ContactFragment)
    fun resolveDependencies(eventsFragment: EventsFragment)
    fun resolveDependencies(jobsFragment: JobsFragment)
    fun resolveDependencies(membersFragment: MembersFragment)
    fun resolveDependencies(loginFragment: LoginFragment)
    fun resolveDependencies(craFragment: CraFragment)
    fun resolveDependencies(craListFragment: CraListFragment)
}