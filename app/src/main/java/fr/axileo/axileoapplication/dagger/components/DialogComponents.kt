package fr.axileo.axileoapplication.dagger.components

import dagger.Subcomponent
import fr.axileo.axileoapplication.features.cra.ui.dialogs.AbsenceDialog
import javax.inject.Singleton

/**
 * Created by BenChaabane on 29/01/2018.
 */
@Subcomponent
@Singleton
interface DialogComponents {
    fun resolveDependencies(absenceDialog: AbsenceDialog)
}