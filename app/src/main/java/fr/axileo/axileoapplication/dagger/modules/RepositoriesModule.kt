package fr.axileo.axileoapplication.dagger.modules

import android.support.annotation.NonNull
import dagger.Module
import dagger.Provides
import fr.axileo.axileoapplication.features.authentication.repositories.AuthenticateRepository
import fr.axileo.axileoapplication.features.authentication.repositories.AuthenticateRepositoryImpl
import fr.axileo.axileoapplication.features.clients.repositories.ClientsRepository
import fr.axileo.axileoapplication.features.clients.repositories.ClientsRepositoryImpl
import fr.axileo.axileoapplication.features.cra.repositories.CraRepository
import fr.axileo.axileoapplication.features.cra.repositories.CraRepositoryImpl
import fr.axileo.axileoapplication.features.events.repositories.EventsRepository
import fr.axileo.axileoapplication.features.events.repositories.EventsRepositoryImpl
import fr.axileo.axileoapplication.features.jobs.repositories.JobsRepository
import fr.axileo.axileoapplication.features.jobs.repositories.JobsRepositoryImpl
import fr.axileo.axileoapplication.features.login.repositories.LoginRepository
import fr.axileo.axileoapplication.features.login.repositories.LoginRepositoryImpl
import fr.axileo.axileoapplication.features.members.repositories.MemberRepository
import fr.axileo.axileoapplication.features.members.repositories.MemberRepositoryImpl
import retrofit2.Retrofit
import javax.inject.Named
import javax.inject.Singleton

/**
 * Created by BenChaabane on 15/01/2018.
 */

@Module
class RepositoriesModule {

    @Singleton
    @Provides
    fun provideEventsRepository(@NonNull retrofit: Retrofit) : EventsRepository {
        return EventsRepositoryImpl(retrofit)
    }

    @Singleton
    @Provides
    fun provideMembersRepository(@NonNull retrofit: Retrofit) : MemberRepository {
        return MemberRepositoryImpl(retrofit)
    }

    @Singleton
    @Provides
    fun provideClientsRepository(@NonNull retrofit: Retrofit) : ClientsRepository {
        return ClientsRepositoryImpl(retrofit)
    }

    @Singleton
    @Provides
    fun provideJobsRepository(@NonNull retrofit: Retrofit) : JobsRepository {
        return JobsRepositoryImpl(retrofit)
    }

    @Singleton
    @Provides
    fun provideAuthenticateRepository(@NonNull @Named("intranet") retrofit: Retrofit) : AuthenticateRepository {
        return AuthenticateRepositoryImpl(retrofit)
    }

    @Singleton
    @Provides
    fun provideLoginRepository(@NonNull @Named("intranet") retrofit: Retrofit) : LoginRepository {
        return LoginRepositoryImpl(retrofit)
    }

    @Singleton
    @Provides
    fun provideCraRepository(@NonNull @Named("intranet") retrofit: Retrofit) : CraRepository {
        return CraRepositoryImpl(retrofit)
    }

}