package fr.axileo.axileoapplication.dagger.modules

import android.support.annotation.NonNull
import dagger.Module
import dagger.Provides
import fr.axileo.axileoapplication.features.authentication.repositories.AuthenticateRepository
import fr.axileo.axileoapplication.features.authentication.usecases.AuthenticateUseCase
import fr.axileo.axileoapplication.features.clients.repositories.ClientsRepository
import fr.axileo.axileoapplication.features.clients.usecases.ClientsUseCase
import fr.axileo.axileoapplication.features.cra.repositories.CraRepository
import fr.axileo.axileoapplication.features.cra.usecases.CraUseCase
import fr.axileo.axileoapplication.features.events.repositories.EventsRepository
import fr.axileo.axileoapplication.features.events.usecases.EventsUseCase
import fr.axileo.axileoapplication.features.jobs.repositories.JobsRepository
import fr.axileo.axileoapplication.features.jobs.usecases.JobsUseCase
import fr.axileo.axileoapplication.features.login.repositories.LoginRepository
import fr.axileo.axileoapplication.features.login.usecase.LoginUseCase
import fr.axileo.axileoapplication.features.members.repositories.MemberRepository
import fr.axileo.axileoapplication.features.members.usecases.MembersUseCase
import javax.inject.Singleton

/**
 * Created by BenChaabane on 15/01/2018.
 */

@Module
class UseCasesModule {

    @Singleton
    @Provides
    fun provideEventsUseCase(@NonNull eventsRepository: EventsRepository) : EventsUseCase {
        return EventsUseCase(eventsRepository)
    }

    @Singleton
    @Provides
    fun provideMembersUseCase(@NonNull memberRepository: MemberRepository) : MembersUseCase {
        return MembersUseCase(memberRepository)
    }

    @Singleton
    @Provides
    fun provideClientsUseCase(@NonNull clientsRepository: ClientsRepository) : ClientsUseCase {
        return ClientsUseCase(clientsRepository)
    }

    @Singleton
    @Provides
    fun provideJobsUseCase(@NonNull jobsRepository: JobsRepository) : JobsUseCase {
        return JobsUseCase(jobsRepository)
    }

    @Singleton
    @Provides
    fun provideAuthenticateUseCase(@NonNull authenticateRepository: AuthenticateRepository) : AuthenticateUseCase {
        return AuthenticateUseCase(authenticateRepository)
    }

    @Singleton
    @Provides
    fun provideLoginUseCase(@NonNull loginRepository: LoginRepository) : LoginUseCase {
        return LoginUseCase(loginRepository)
    }

    @Singleton
    @Provides
    fun provideCraUseCase(@NonNull craRepository: CraRepository) : CraUseCase {
        return CraUseCase(craRepository)
    }
}