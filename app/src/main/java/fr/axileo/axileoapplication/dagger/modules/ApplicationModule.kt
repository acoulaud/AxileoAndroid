package fr.axileo.axileoapplication.dagger.modules

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import io.reactivex.annotations.NonNull
import org.greenrobot.eventbus.EventBus
import javax.inject.Singleton

/**
 * Created by BenChaabane on 15/01/2018.
 */

@Module
class ApplicationModule (@NonNull val application: Application) {

    @Singleton
    @Provides
    fun provideContext(): Context {
        return this.application
    }

    @Singleton
    @Provides
    fun provideGson(): Gson {
        return GsonBuilder().serializeNulls().create()
    }

    @Singleton
    @Provides
    fun provideEventBus(): EventBus {
        return EventBus.getDefault()
    }

    @Singleton
    @Provides
    fun provideSharedPrefs(context: Context) : SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(context)
    }

}