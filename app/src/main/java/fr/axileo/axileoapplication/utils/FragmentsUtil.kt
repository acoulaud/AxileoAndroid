package fr.axileo.axileoapplication.utils

import android.support.annotation.NonNull
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import fr.axileo.axileoapplication.R

/**
 * Created by BenChaabane on 16/01/2018.
 */
class FragmentsUtil {

    companion object {
        fun setFragment(@NonNull context: AppCompatActivity, @NonNull frag: Fragment,
                        @NonNull tag: String, @NonNull containerId: Int,
                        @NonNull addToBackStack: Boolean) {
            val transaction = context.supportFragmentManager.beginTransaction()
            transaction.replace(containerId, frag, tag)
            transaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
            if (addToBackStack) transaction.addToBackStack(tag)
            transaction.commit()
        }
    }
}