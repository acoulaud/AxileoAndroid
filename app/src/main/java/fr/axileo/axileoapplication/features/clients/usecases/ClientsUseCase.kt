package fr.axileo.axileoapplication.features.clients.usecases

import fr.axileo.axileoapplication.features.clients.repositories.ClientsRepository

/**
 * Created by BenChaabane on 22/01/2018.
 */
class ClientsUseCase(private var clientsRepository: ClientsRepository) {

    fun getClients() = clientsRepository.getClients()
}