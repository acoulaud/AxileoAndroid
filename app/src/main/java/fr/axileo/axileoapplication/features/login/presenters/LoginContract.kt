package fr.axileo.axileoapplication.features.login.presenters

import android.arch.lifecycle.Lifecycle
import fr.axileo.axileoapplication.features.login.entities.Account

/**
 * Created by BenChaabane on 25/01/2018.
 */
interface LoginContract {

    interface Presenter {
        fun attachPresenter(view: View, lifecycle: Lifecycle)
        fun login(login: String, password: String, token: String)
        fun changePassword(login: String, token: String)
    }

    interface View {
        fun onUserLogged(account: Account)
        fun onTokenInvalid()
        fun onLoginFailed()
        fun onPasswordChangeRequestSucceded()
        fun onPasswordChangeRequestFailed()
    }
}