package fr.axileo.axileoapplication.features.jobs.adapters

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import fr.axileo.axileoapplication.R
import fr.axileo.axileoapplication.features.jobs.entities.Job

/**
 * Created by BenChaabane on 22/01/2018.
 */
class JobsAdapter(var jobs: ArrayList<Job>) : RecyclerView.Adapter<JobsAdapter.JobViewHolder>(){

    override fun getItemCount(): Int {
        return jobs.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): JobViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_job, parent, false)
        return JobViewHolder(view)
    }

    override fun onBindViewHolder(holder: JobViewHolder, position: Int) {
        if (position < jobs.size && position != RecyclerView.NO_POSITION) {
            val job = jobs[position]
            holder.bindViewHolder(job)
            holder.apply.setOnClickListener({sendMailToAxileo(holder.itemView.context, job)})
            holder.link.setOnClickListener({openJobLink(holder.itemView.context, job)})
        }
    }

    fun updateData(jobs: ArrayList<Job>) {
        this.jobs = jobs
        notifyDataSetChanged()
    }

    fun openJobLink(context: Context, job: Job) {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(job.link)
        if (intent.resolveActivity(context.packageManager) != null) {
            context.startActivity(intent)
        }
    }

    fun sendMailToAxileo(context: Context, job: Job) {
        val intent = Intent(Intent.ACTION_SENDTO)
        with (intent) {
            type = "text/plain"
            data = Uri.parse("mailto:")
            putExtra(Intent.EXTRA_EMAIL, arrayOf("drh@axileo.fr"))
            putExtra(Intent.EXTRA_SUBJECT, job.title)
        }
        if (intent.resolveActivity(context.packageManager) != null) {
            context.startActivity(Intent.createChooser(intent, context.getString(R.string.share_chooser)))
        }

    }

    class JobViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @BindView(R.id.job_title) lateinit var title: TextView
        @BindView(R.id.job_location) lateinit var location: TextView
        @BindView(R.id.job_type) lateinit var type: TextView
        @BindView(R.id.job_description) lateinit var description: TextView
        @BindView(R.id.job_apply) lateinit var apply: TextView
        @BindView(R.id.job_link) lateinit var link: TextView

        init {
            ButterKnife.bind(this, itemView)
        }

        fun bindViewHolder(job: Job) {
            title.text = job.title
            location.text = job.location
            type.text = job.type
            description.text = job.description
        }
    }
}