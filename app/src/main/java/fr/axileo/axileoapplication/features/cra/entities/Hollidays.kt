package fr.axileo.axileoapplication.features.cra.entities

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created by BenChaabane on 30/01/2018.
 */
data class Hollidays(@SerializedName("holidays") var holidays: ArrayList<Long>) : Parcelable {

    constructor(parcel: Parcel) : this(parcel.readArrayList(Long::class.java.classLoader) as ArrayList<Long>)

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeList(holidays)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Hollidays> {
        override fun createFromParcel(parcel: Parcel): Hollidays {
            return Hollidays(parcel)
        }

        override fun newArray(size: Int): Array<Hollidays?> {
            return arrayOfNulls(size)
        }
    }
}