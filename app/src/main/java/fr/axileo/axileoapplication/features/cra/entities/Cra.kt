package fr.axileo.axileoapplication.features.cra.entities

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import fr.axileo.axileoapplication.features.login.entities.UserInfo

/**
 * Created by BenChaabane on 29/01/2018.
 */
data class Cra(@SerializedName("user") var userInfo: UserInfo,
               @SerializedName("RA") var craMonth: CraMonth) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readParcelable(UserInfo::class.java.classLoader),
            parcel.readParcelable(CraMonth::class.java.classLoader))

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(userInfo, flags)
        parcel.writeParcelable(craMonth, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Cra> {
        override fun createFromParcel(parcel: Parcel): Cra {
            return Cra(parcel)
        }

        override fun newArray(size: Int): Array<Cra?> {
            return arrayOfNulls(size)
        }
    }
}