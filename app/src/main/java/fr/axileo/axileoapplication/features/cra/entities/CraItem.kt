package fr.axileo.axileoapplication.features.cra.entities

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created by BenChaabane on 30/01/2018.
 */
data class CraItem(@SerializedName("id") var id: Int,
                   @SerializedName("mois") var month: String,
                   @SerializedName("annee") var year: String) : Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readString(),
            parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(month)
        parcel.writeString(year)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CraItem> {
        override fun createFromParcel(parcel: Parcel): CraItem {
            return CraItem(parcel)
        }

        override fun newArray(size: Int): Array<CraItem?> {
            return arrayOfNulls(size)
        }
    }
}