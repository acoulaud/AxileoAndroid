package fr.axileo.axileoapplication.features.cra.presenters

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.OnLifecycleEvent
import android.content.Context
import android.util.Log
import fr.axileo.axileoapplication.features.cra.entities.CraItem
import fr.axileo.axileoapplication.features.cra.usecases.CraUseCase
import fr.axileo.axileoapplication.features.login.entities.User
import fr.axileo.axileoapplication.launchers.AxileoApplication
import io.reactivex.CompletableObserver
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import retrofit2.HttpException
import java.net.HttpURLConnection
import javax.inject.Inject

/**
 * Created by BenChaabane on 30/01/2018.
 */
class CraListPresenter(var context: Context) : CraListContract.Presenter, LifecycleObserver {

    @Inject lateinit var craUseCase: CraUseCase
    lateinit var view : CraListContract.View
    val compositeDisposable = CompositeDisposable()

    init {
        AxileoApplication.getApplication(context).getPresentersComponent().resolveDependencies(this)
    }

    override fun attachPresenter(view: CraListContract.View, lifecycle: Lifecycle) {
        this.view = view
        lifecycle.addObserver(this)
    }

    override fun retrieveAllCra(token: String, user: User) {
        craUseCase.retrieveAllCra(token, user)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : SingleObserver<ArrayList<CraItem>> {
                    override fun onSuccess(t: ArrayList<CraItem>) {
                        view.onAllCraRetrieved(t)
                    }

                    override fun onSubscribe(d: Disposable) {
                        if (!d.isDisposed) compositeDisposable.add(d)
                    }

                    override fun onError(e: Throwable) {
                        if (e is HttpException) {
                            when (e.code()) {
                                HttpURLConnection.HTTP_UNAUTHORIZED -> { view.onTokenInvalid() }
                                else -> view.onAllCraFailure(e)
                            }
                        } else view.onAllCraFailure(e)
                    }

                })
    }

    override fun deleteCra(token: String, craId: String, user: User) {
        craUseCase.deleteCra(token, craId, user)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : CompletableObserver{
                    override fun onComplete() {
                        view.onCraDeleted()
                    }

                    override fun onSubscribe(d: Disposable) {
                        if (!d.isDisposed) compositeDisposable.add(d)
                    }

                    override fun onError(e: Throwable) {
                        Log.e("##### DELETE CRA", e.message, e)
                        view.onCraDeleteFailure(e)
                    }

                })
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun detachPresenter() {
        compositeDisposable.clear()
    }

}