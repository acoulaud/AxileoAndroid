package fr.axileo.axileoapplication.features.cra.entities

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created by BenChaabane on 29/01/2018.
 */
data class CraMonth(@SerializedName("mois") var month: String,
                    @SerializedName("annee") var year: String,
                    @SerializedName("societe") var company: String,
                    @SerializedName("site") var location: String,
                    @SerializedName("materiel") var material: Boolean,
                    @SerializedName("sauvegarde") var materialSave: Boolean,
                    @SerializedName("commentaires") var comments: String,
                    @SerializedName("jours") var craDays: ArrayList<CraDay>) : Parcelable {
    
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readByte() != 0.toByte(),
            parcel.readByte() != 0.toByte(),
            parcel.readString(),
            parcel.readArrayList(CraDay::class.java.classLoader) as ArrayList<CraDay>)

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(month)
        parcel.writeString(year)
        parcel.writeString(company)
        parcel.writeString(location)
        parcel.writeByte(if (material) 1 else 0)
        parcel.writeByte(if (materialSave) 1 else 0)
        parcel.writeString(comments)
        parcel.writeList(craDays)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CraMonth> {
        override fun createFromParcel(parcel: Parcel): CraMonth {
            return CraMonth(parcel)
        }

        override fun newArray(size: Int): Array<CraMonth?> {
            return arrayOfNulls(size)
        }
    }
}
