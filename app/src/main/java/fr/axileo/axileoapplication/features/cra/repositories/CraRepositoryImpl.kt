package fr.axileo.axileoapplication.features.cra.repositories

import fr.axileo.axileoapplication.features.cra.entities.Cra
import fr.axileo.axileoapplication.features.cra.entities.CraItem
import fr.axileo.axileoapplication.features.cra.entities.Hollidays
import fr.axileo.axileoapplication.features.login.entities.User
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import javax.inject.Singleton

/**
 * Created by BenChaabane on 29/01/2018.
 */

@Singleton
class CraRepositoryImpl(var retrofit: Retrofit) : CraRepository {

    val craService : CraRestRepository = retrofit.create(CraRestRepository::class.java)

    override fun addCra(token: String, cra: Cra): Completable {
        return craService.addCra(token, cra)
                .subscribeOn(Schedulers.io())
    }

    override fun getHolidays(token: String, year: String): Single<Hollidays> {
        return craService.getHolidays(token, year)
                .subscribeOn(Schedulers.io())
    }

    override fun retrieveAllCra(token: String, user: User): Single<ArrayList<CraItem>> {
        return craService.retrieveAllCra(token, user)
                .subscribeOn(Schedulers.io())
    }

    override fun deleteCra(token: String, craId: String, user: User): Completable {
        return craService.deleteCra(token, craId, user)
                .subscribeOn(Schedulers.io())
    }
}