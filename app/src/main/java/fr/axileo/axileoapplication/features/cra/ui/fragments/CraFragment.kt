package fr.axileo.axileoapplication.features.cra.ui.fragments

import android.content.SharedPreferences
import android.os.Bundle
import android.support.design.widget.TextInputEditText
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.support.v7.widget.AppCompatCheckBox
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ProgressBar
import butterknife.BindView
import butterknife.ButterKnife
import com.applikeysolutions.cosmocalendar.model.Day
import com.applikeysolutions.cosmocalendar.view.CalendarView
import fr.axileo.axileoapplication.R
import fr.axileo.axileoapplication.constants.SharedPreferencesConstants.Companion.TOKEN_SHARED_PREFERENCE
import fr.axileo.axileoapplication.constants.SharedPreferencesConstants.Companion.USER_LOGIN
import fr.axileo.axileoapplication.constants.SharedPreferencesConstants.Companion.USER_PASSWORD
import fr.axileo.axileoapplication.events.AbsenceMotifEvent
import fr.axileo.axileoapplication.events.UpdateToolbarTitleEvent
import fr.axileo.axileoapplication.features.authentication.constants.AuthenticationConstants.Companion.HEADER_BEARER
import fr.axileo.axileoapplication.features.authentication.entities.OauthToken
import fr.axileo.axileoapplication.features.authentication.presenters.AuthenticateContract
import fr.axileo.axileoapplication.features.authentication.presenters.AuthenticatePresenter
import fr.axileo.axileoapplication.features.cra.entities.Cra
import fr.axileo.axileoapplication.features.cra.entities.CraDay
import fr.axileo.axileoapplication.features.cra.entities.CraMonth
import fr.axileo.axileoapplication.features.cra.entities.Hollidays
import fr.axileo.axileoapplication.features.cra.presenters.CraContract
import fr.axileo.axileoapplication.features.cra.presenters.CraPresenter
import fr.axileo.axileoapplication.features.cra.ui.dialogs.AbsenceDialog
import fr.axileo.axileoapplication.features.login.entities.UserInfo
import fr.axileo.axileoapplication.launchers.AxileoApplication
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList
import kotlin.collections.HashSet


/**
 * Created by BenChaabane on 26/01/2018.
 */
class CraFragment : Fragment(), CalendarView.DayToggleListener, CraContract.View, AuthenticateContract.View {
    @BindView(R.id.calendarView) lateinit var calendar: CalendarView
    @BindView(R.id.cra_client) lateinit var clientEditTxt: TextInputEditText
    @BindView(R.id.cra_site) lateinit var siteEditText: TextInputEditText
    @BindView(R.id.cra_material) lateinit var materialChbx: AppCompatCheckBox
    @BindView(R.id.cra_material_save) lateinit var saveChbx: AppCompatCheckBox
    @BindView(R.id.cra_observation) lateinit var observationEditText: TextInputEditText
    @BindView(R.id.cra_validate_btn) lateinit var validateCraBtn: Button
    @BindView(R.id.cra_progressBar2) lateinit var progressBar: ProgressBar

    lateinit var craDays: ArrayList<CraDay>
    lateinit var craPresenter: CraPresenter
    lateinit var authenticatePresenter: AuthenticatePresenter
    lateinit var token: String
    lateinit var login: String
    lateinit var password: String
    var isFromHolidays: Boolean = false

    @Inject lateinit var sharedPrefs: SharedPreferences
    @Inject lateinit var eventBus: EventBus

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AxileoApplication.getApplication(context).getFragmentsComponent().resolveDependencies(this)
        if (savedInstanceState != null) {
            craDays = savedInstanceState.getParcelableArrayList(BUNDLE_CRA_DAYS)
        } else {
            craDays = ArrayList()
        }
        login = sharedPrefs.getString(USER_LOGIN, "")
        password = sharedPrefs.getString(USER_PASSWORD, "")
        token = sharedPrefs.getString(TOKEN_SHARED_PREFERENCE, "")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_cra, container, false)
        ButterKnife.bind(this, view)
        craPresenter = CraPresenter(context)
        craPresenter.attachPresenter(this, lifecycle)
        authenticatePresenter = AuthenticatePresenter(context)
        authenticatePresenter.attachPresenter(this, lifecycle)
        craPresenter.getHolidays("$HEADER_BEARER$token", "${Calendar.getInstance().get(Calendar.YEAR)}")

        calendar.setDaysToggleListener(this)
        validateCraBtn.setOnClickListener({if (checkInfoBeforeValidation()) createCra()})

        return view
    }

    override fun onResume() {
        super.onResume()
        if (!eventBus.isRegistered(this)) {
            eventBus.register(this)
        }
        eventBus.post(UpdateToolbarTitleEvent(getString(R.string.cra)))
    }

    override fun onStop() {
        super.onStop()
        if (eventBus.isRegistered(this)) {
            eventBus.unregister(this)
        }
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putParcelableArrayList(BUNDLE_CRA_DAYS, craDays)
    }

    override fun onDayRemoved(day: Day?) {
        val craDay = CraDay("${day?.dayNumber}","1")
        if (craDays.contains(craDay)) {
            craDays.remove(craDay)
        }
        val absenceDialog = AbsenceDialog(context, craDay)
        absenceDialog.show()
    }

    override fun onDayAdded(day: Day?) {
        val craDay = CraDay("${day?.dayNumber}","1")
        if (!craDays.contains(craDay)) {
            craDays.add(craDay)
        } else {
            val index = craDays.indexOf(craDay)
            craDays[index] = craDay
        }
    }

    override fun onCraAdded() {
        progressBar.visibility = View.GONE
        if (isAdded) {
            AlertDialog.Builder(activity)
                    .setMessage(R.string.cra_success)
                    .setPositiveButton(android.R.string.ok) { dialog, which -> dialog.dismiss()}
                    .create()
                    .show()
        }
    }

    override fun onTokenInvalid() {
        authenticatePresenter.authenticate()
    }

    override fun onCraFailure(throwable: Throwable) {
        progressBar.visibility = View.GONE
        if (isAdded) {
            AlertDialog.Builder(activity)
                    .setMessage(R.string.cra_failed)
                    .setPositiveButton(android.R.string.ok) { dialog, which -> dialog.dismiss()}
                    .create()
                    .show()
        }
    }

    override fun onAuthenticationSuccess(oauthToken: OauthToken) {
        sharedPrefs.edit().putString(TOKEN_SHARED_PREFERENCE, oauthToken.accessToken).apply()
        token = oauthToken.accessToken
        if (isFromHolidays) {
            craPresenter.getHolidays("$HEADER_BEARER$token", "${Calendar.getInstance().get(Calendar.YEAR)}")
            isFromHolidays = false
        } else {
            validateCraBtn.performClick()
        }

    }

    override fun onAuthenticationFailed(throwable: Throwable) {
        progressBar.visibility = View.GONE
        if (isAdded) {
            AlertDialog.Builder(activity)
                    .setMessage(R.string.splash_login_warning_text)
                    .setPositiveButton(android.R.string.ok) { dialog, which -> dialog.dismiss()}
                    .create()
                    .show()
        }
    }

    override fun onHolidaysRetrieved(holidays: Hollidays) {
        val disabledDays = HashSet<Long>()
        val currentMonth = Calendar.getInstance().get(Calendar.MONTH)
        for (timestamp: Long in holidays.holidays) {
            val date = Date(timestamp)
            if (date.month == currentMonth) disabledDays.add(timestamp)
        }
        calendar.disableDays(disabledDays)
    }

    override fun onHolidayTokenInvalid() {
        isFromHolidays = true
        authenticatePresenter.authenticate()
    }

    override fun onHolidaysFailure(throwable: Throwable) {
    }

    @SuppressWarnings("unused")
    @Subscribe
    fun onAbsenceSelected(event: AbsenceMotifEvent) {
        if (!craDays.contains(event.craDay)) {
            event.craDay.project = ""
            craDays.add(event.craDay)
        }
    }

    fun checkInfoBeforeValidation() : Boolean {
        if (clientEditTxt.editableText.isEmpty() || siteEditText.editableText.isEmpty()) {
            clientEditTxt.error = getString(R.string.missed_field)
            siteEditText.error = getString(R.string.missed_field)
            if (isAdded) {
                AlertDialog.Builder(activity)
                        .setMessage(R.string.warning_missed_info)
                        .setPositiveButton(android.R.string.ok) { dialog, which -> dialog.dismiss()}
                        .create()
                        .show()
            }
            return false
        }
        return true
    }

    fun createCra() {
        progressBar.visibility = View.VISIBLE
        val userInfo = UserInfo(login, password)
        val company = clientEditTxt.editableText.toString()
        val site = siteEditText.editableText.toString()
        val material = materialChbx.isChecked
        val materialSave = saveChbx.isChecked
        val comments = observationEditText.editableText.toString()
        val month = "${Calendar.getInstance().get(Calendar.MONTH) + 1}"
        val year = "${Calendar.getInstance().get(Calendar.YEAR)}"
        val craMonth = CraMonth(month,year,company,site,material,materialSave,comments,craDays)
        val cra = Cra(userInfo, craMonth)
        craPresenter.addCra("$HEADER_BEARER$token", cra)
    }

    companion object {
        val TAG = CraFragment::class.java.simpleName
        val BUNDLE_CRA_DAYS = TAG + ":BUNDLE_CRA_DAYS"

        fun newInstance() =  CraFragment ()

    }
}