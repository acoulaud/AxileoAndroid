package fr.axileo.axileoapplication.features.events.presenters

import android.arch.lifecycle.Lifecycle
import fr.axileo.axileoapplication.features.events.entities.Event

/**
 * Created by BenChaabane on 15/01/2018.
 */

interface EventsContract {

    interface Presenter {
        fun attachPresenter(view: EventsContract.View, lifeCycle: Lifecycle)
        fun getEvents()
    }

    interface View {
        fun onEventsRetrieved(events : ArrayList<Event>)
        fun onEventsError(throwable: Throwable)
    }

}