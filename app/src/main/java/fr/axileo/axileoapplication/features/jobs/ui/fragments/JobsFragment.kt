package fr.axileo.axileoapplication.features.jobs.ui.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.BindView
import butterknife.ButterKnife
import fr.axileo.axileoapplication.R
import fr.axileo.axileoapplication.events.UpdateToolbarTitleEvent
import fr.axileo.axileoapplication.features.jobs.adapters.JobsAdapter
import fr.axileo.axileoapplication.features.jobs.entities.Job
import fr.axileo.axileoapplication.features.jobs.presenters.JobsContract
import fr.axileo.axileoapplication.features.jobs.presenters.JobsPresenter
import fr.axileo.axileoapplication.launchers.AxileoApplication
import org.greenrobot.eventbus.EventBus
import javax.inject.Inject

/**
 * Created by BenChaabane on 22/01/2018.
 */
class JobsFragment : Fragment(), JobsContract.View {
    @BindView(R.id.jobs_recyclerView) lateinit var jobsRecyclerView: RecyclerView
    @Inject lateinit var eventBus: EventBus

    lateinit var jobs: ArrayList<Job>
    lateinit var jobsAdapter: JobsAdapter
    lateinit var presenter: JobsPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AxileoApplication.getApplication(context).getFragmentsComponent().resolveDependencies(this)
        jobs = savedInstanceState?.getParcelableArrayList(JOBS_BUNDLE) ?: ArrayList()
        jobsAdapter = JobsAdapter(jobs)
        presenter = JobsPresenter(context)
        presenter.attachPresenter(this, lifecycle)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_jobs, container, false)
        ButterKnife.bind(this, view)
        with(jobsRecyclerView) {
            hasFixedSize()
            adapter = jobsAdapter
        }
        if (jobs.size == 0) presenter.getJobs()
        return view
    }

    override fun onResume() {
        super.onResume()
        eventBus.post(UpdateToolbarTitleEvent(getString(R.string.offers)))
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putParcelableArrayList(JOBS_BUNDLE, jobs)
    }

    override fun onJobsRetrieved(jobs: ArrayList<Job>) {
        jobsAdapter.updateData(jobs)
    }

    override fun onJobError(throwable: Throwable) {
        if (isAdded) {
            AlertDialog.Builder(context)
                    .setMessage(R.string.splash_warning_text)
                    .setOnDismissListener({activity.supportFragmentManager.popBackStackImmediate()})
                    .create()
                    .show()
        }
    }

    companion object {
        val TAG = JobsFragment::class.java.simpleName
        val JOBS_BUNDLE = TAG + ":JOBS_BUNDLE"

        fun newInstance() = JobsFragment()
    }
}