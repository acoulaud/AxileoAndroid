package fr.axileo.axileoapplication.features.members.presenters

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.OnLifecycleEvent
import android.content.Context
import fr.axileo.axileoapplication.features.members.entities.Member
import fr.axileo.axileoapplication.features.members.usecases.MembersUseCase
import fr.axileo.axileoapplication.launchers.AxileoApplication
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import javax.inject.Inject

/**
 * Created by BenChaabane on 19/01/2018.
 */
class MembersPresenter(var context: Context) : MembersContract.Presenter, LifecycleObserver {
    @Inject lateinit var memberUseCase: MembersUseCase
    lateinit var view: MembersContract.View
    private val compositeDisposable: CompositeDisposable

    init {
        AxileoApplication.getApplication(context).getPresentersComponent().resolveDependencies(this)
        compositeDisposable = CompositeDisposable()
    }

    override fun attachPresenter(view: MembersContract.View, lifecycle: Lifecycle) {
        this.view = view
        lifecycle.addObserver(this)
    }

    override fun getMembers() {
        memberUseCase.getMembers()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : SingleObserver<ArrayList<Member>> {
                    override fun onSubscribe(d: Disposable) {
                        if (!d.isDisposed) compositeDisposable.add(d)
                    }

                    override fun onSuccess(t: ArrayList<Member>) {
                        view.onMembersRetrieved(t)
                    }

                    override fun onError(e: Throwable) {
                        view.onMembersError(e)
                    }
                })
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun detachPresenter() {
        compositeDisposable.clear()
    }
}