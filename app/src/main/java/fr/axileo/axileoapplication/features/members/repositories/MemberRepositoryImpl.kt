package fr.axileo.axileoapplication.features.members.repositories

import fr.axileo.axileoapplication.features.members.entities.Member
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import javax.inject.Singleton

/**
 * Created by BenChaabane on 19/01/2018.
 */
@Singleton
class MemberRepositoryImpl(retrofit: Retrofit) : MemberRepository {

    val memberService: MemberRestRepository = retrofit.create(MemberRestRepository::class.java)

    override fun getMembers(): Single<ArrayList<Member>> {
        return memberService.getMembers("")
                .subscribeOn(Schedulers.io())
    }
}