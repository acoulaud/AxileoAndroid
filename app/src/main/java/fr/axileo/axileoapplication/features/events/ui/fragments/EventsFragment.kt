package fr.axileo.axileoapplication.features.events.ui.fragments

import android.os.Bundle
import android.support.annotation.NonNull
import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.BindView
import butterknife.ButterKnife
import fr.axileo.axileoapplication.R
import fr.axileo.axileoapplication.events.UpdateToolbarTitleEvent
import fr.axileo.axileoapplication.features.events.adapters.EventsAdapter
import fr.axileo.axileoapplication.features.events.entities.Event
import fr.axileo.axileoapplication.launchers.AxileoApplication
import org.greenrobot.eventbus.EventBus
import javax.inject.Inject

/**
 * Created by BenChaabane on 16/01/2018.
 */
class EventsFragment : Fragment() {

    @BindView(R.id.events_recyclerView) lateinit var eventsRecyclerView: RecyclerView

    @Inject lateinit var eventBus: EventBus

    lateinit var events: ArrayList<Event>
    lateinit var eventsAdapter: EventsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AxileoApplication.getApplication(context).getFragmentsComponent().resolveDependencies(this)
        val creator = savedInstanceState ?: arguments
        events = creator.getParcelableArrayList(EVENTS_ARGS)
        eventsAdapter = EventsAdapter(events)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_events, container, false)
        ButterKnife.bind(this, view)
        with(eventsRecyclerView) {
            eventsRecyclerView.hasFixedSize()
            eventsRecyclerView.adapter = eventsAdapter
        }
        return view
    }

    override fun onResume() {
        super.onResume()
        eventBus.post(UpdateToolbarTitleEvent(getString(R.string.events)))
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putParcelableArrayList(EVENTS_ARGS, events)
    }

    companion object {
        val TAG = EventsFragment::class.java.simpleName
        private val EVENTS_ARGS = TAG + ":EVENTS_ARGS"

        fun newInstance(@NonNull events: ArrayList<Event>) : EventsFragment {
            val frag = EventsFragment()
            val args = Bundle()
            args.putParcelableArrayList(EVENTS_ARGS, events)
            frag.arguments = args
            return frag
        }

    }
}