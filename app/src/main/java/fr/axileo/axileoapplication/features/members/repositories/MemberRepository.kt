package fr.axileo.axileoapplication.features.members.repositories

import fr.axileo.axileoapplication.features.members.entities.Member
import io.reactivex.Single

/**
 * Created by BenChaabane on 19/01/2018.
 */
interface MemberRepository {

    fun getMembers(): Single<ArrayList<Member>>
}