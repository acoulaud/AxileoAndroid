package fr.axileo.axileoapplication.features.events.repositories

import fr.axileo.axileoapplication.features.events.entities.Event
import io.reactivex.Single

/**
 * Created by BenChaabane on 15/01/2018.
 */

interface EventsRepository {

    fun getEvents() : Single<ArrayList<Event>>
}