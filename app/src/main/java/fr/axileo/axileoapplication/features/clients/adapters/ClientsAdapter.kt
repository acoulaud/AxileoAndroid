package fr.axileo.axileoapplication.features.clients.adapters

import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.drawee.interfaces.DraweeController
import com.facebook.drawee.view.SimpleDraweeView
import com.facebook.imagepipeline.request.ImageRequest
import com.facebook.imagepipeline.request.ImageRequestBuilder
import fr.axileo.axileoapplication.R
import fr.axileo.axileoapplication.features.clients.entities.Client


/**
 * Created by BenChaabane on 22/01/2018.
 */
class ClientsAdapter(var clients: ArrayList<Client>) : RecyclerView.Adapter<ClientsAdapter.ClientViewHolder> (){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ClientViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_client, parent, false)
        return ClientViewHolder(view)
    }

    override fun onBindViewHolder(holder: ClientViewHolder, position: Int) {
        if (position < clients.size && position != RecyclerView.NO_POSITION) {
            val client = clients[position]
            holder.bindViewHolder(client)
        }
    }

    override fun getItemCount(): Int {
        return clients.size
    }

    fun updateData(clients: ArrayList<Client>) {
        this.clients = clients
        notifyDataSetChanged()
    }

    class ClientViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @BindView(R.id.client_logo) lateinit var clientLogo: SimpleDraweeView
        @BindView(R.id.client_name) lateinit var clientName: TextView
        @BindView(R.id.client_description) lateinit var clientDescription: TextView
        lateinit var imageRequest: ImageRequest
        lateinit var draweeController: DraweeController
        init {
            ButterKnife.bind(this, itemView)
        }
        fun bindViewHolder(client: Client) {
            imageRequest = ImageRequestBuilder.newBuilderWithSource(Uri.parse(client.logo))
                    .setProgressiveRenderingEnabled(true)
                    .build()

            draweeController = Fresco.newDraweeControllerBuilder()
                    .setImageRequest(imageRequest)
                    .setOldController(clientLogo.controller)
                    .build()
            clientLogo.controller = draweeController
            clientName.text = client.name
            clientDescription.text = client.description
        }
    }
}