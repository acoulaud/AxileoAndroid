package fr.axileo.axileoapplication.features.login.ui

import android.content.SharedPreferences
import android.os.Bundle
import android.support.design.widget.TextInputEditText
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import fr.axileo.axileoapplication.R
import fr.axileo.axileoapplication.constants.SharedPreferencesConstants.Companion.TOKEN_SHARED_PREFERENCE
import fr.axileo.axileoapplication.constants.SharedPreferencesConstants.Companion.USER_LOGIN
import fr.axileo.axileoapplication.constants.SharedPreferencesConstants.Companion.USER_PASSWORD
import fr.axileo.axileoapplication.events.UpdateToolbarTitleEvent
import fr.axileo.axileoapplication.events.UserLoggedEvent
import fr.axileo.axileoapplication.features.authentication.constants.AuthenticationConstants.Companion.HEADER_BEARER
import fr.axileo.axileoapplication.features.authentication.entities.OauthToken
import fr.axileo.axileoapplication.features.authentication.presenters.AuthenticateContract
import fr.axileo.axileoapplication.features.authentication.presenters.AuthenticatePresenter
import fr.axileo.axileoapplication.features.login.entities.Account
import fr.axileo.axileoapplication.features.login.entities.AccountInfo
import fr.axileo.axileoapplication.features.login.presenters.LoginContract
import fr.axileo.axileoapplication.features.login.presenters.LoginPresenter
import fr.axileo.axileoapplication.launchers.AxileoApplication
import org.greenrobot.eventbus.EventBus
import javax.inject.Inject

/**
 * Created by BenChaabane on 25/01/2018.
 */
class LoginFragment : Fragment(), LoginContract.View, AuthenticateContract.View {

    @BindView(R.id.login_form_layout) lateinit var loginForm: LinearLayout
    @BindView(R.id.login_mail_edit) lateinit var mailEditTxt: TextInputEditText
    @BindView(R.id.login_password_edit) lateinit var passwordEditTxt: TextInputEditText
    @BindView(R.id.login_connection_btn) lateinit var connectionBtn: Button
    @BindView(R.id.password_forgotten_txt) lateinit var passwordForgottenTxt: TextView
    @BindView(R.id.progressBarConnection) lateinit var progress: ProgressBar


    @BindView(R.id.account_info_layout) lateinit var infoLayout: LinearLayout
    @BindView(R.id.account_first_name) lateinit var firstNameTxt: TextView
    @BindView(R.id.account_last_name) lateinit var lastNameTxt: TextView
    @BindView(R.id.account_position) lateinit var positionTxt: TextView
    @BindView(R.id.account_entry_date) lateinit var entryDateTxt: TextView
    @BindView(R.id.account_mail) lateinit var mailTxt: TextView
    @BindView(R.id.account_trigram) lateinit var trigramTxt: TextView

    @Inject lateinit var eventBus: EventBus
    @Inject lateinit var sharedPreferences: SharedPreferences

    var account: Account? = null
    lateinit var loginPresenter: LoginPresenter
    lateinit var authenticatePresenter: AuthenticatePresenter
    lateinit var token: String


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AxileoApplication.getApplication(context).getFragmentsComponent().resolveDependencies(this)
        val creator = savedInstanceState?: arguments
        account = creator.getParcelable(ACCOUNT_BUNDLE)
        token = sharedPreferences.getString(TOKEN_SHARED_PREFERENCE, "")

        loginPresenter = LoginPresenter(context)
        loginPresenter.attachPresenter(this, lifecycle)

        authenticatePresenter = AuthenticatePresenter(context)
        authenticatePresenter.attachPresenter(this, lifecycle)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_login, container, false)
        ButterKnife.bind(this, view)
        if (account != null) {
            populateAccountInfo(account!!.accountInfo)
        }

        connectionBtn.setOnClickListener({connect()})

        passwordForgottenTxt.setOnClickListener({loginPresenter.changePassword(mailEditTxt.editableText.toString(),
                "$HEADER_BEARER$token")})
        return view
    }

    override fun onResume() {
        super.onResume()
        eventBus.post(UpdateToolbarTitleEvent(getString(if (account!= null) {
            R.string.account
        } else R.string.connection)))
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putParcelable(ACCOUNT_BUNDLE, account)
    }

    override fun onUserLogged(account: Account) {
        sharedPreferences.edit().putString(USER_LOGIN, mailEditTxt.editableText.toString()).apply()
        sharedPreferences.edit().putString(USER_PASSWORD, passwordEditTxt.editableText.toString()).apply()
        populateAccountInfo(account.accountInfo)
        eventBus.post(UserLoggedEvent(account))
        eventBus.post(UpdateToolbarTitleEvent(getString(
            R.string.account)))
    }

    override fun onTokenInvalid() {
        authenticatePresenter.authenticate()
    }

    override fun onLoginFailed() {
        progress.visibility = View.GONE
        if (isAdded) {
            AlertDialog.Builder(activity)
                    .setMessage(R.string.splash_login_warning_text)
                    .setPositiveButton(android.R.string.ok) { dialog, which -> dialog.dismiss()}
                    .create()
                    .show()
        }
    }

    override fun onPasswordChangeRequestSucceded() {
        if (isAdded) {
            AlertDialog.Builder(activity)
                    .setMessage(R.string.refresh_password_succeeded)
                    .setPositiveButton(android.R.string.ok) { dialog, which -> dialog.dismiss()}
                    .create()
                    .show()
        }
    }

    override fun onPasswordChangeRequestFailed() {
        if (isAdded) {
            AlertDialog.Builder(activity)
                    .setMessage(R.string.refresh_password_failed)
                    .setPositiveButton(android.R.string.ok) { dialog, which -> dialog.dismiss()}
                    .create()
                    .show()
        }
    }

    override fun onAuthenticationSuccess(oauthToken: OauthToken) {
        sharedPreferences.edit().putString(TOKEN_SHARED_PREFERENCE, oauthToken.accessToken).apply()
        token = oauthToken.accessToken
        connectionBtn.performClick()
    }

    override fun onAuthenticationFailed(throwable: Throwable) {
        progress.visibility = View.GONE
        if (isAdded) {
            AlertDialog.Builder(activity)
                    .setMessage(R.string.splash_login_warning_text)
                    .setPositiveButton(android.R.string.ok) { dialog, which -> dialog.dismiss()}
                    .create()
                    .show()
        }
    }

    private fun populateAccountInfo(accountInfo: AccountInfo) {
        loginForm.visibility = View.GONE
        infoLayout.visibility = View.VISIBLE
        firstNameTxt.text = accountInfo.name
        lastNameTxt.text = accountInfo.lastName
        positionTxt.text = accountInfo.position
        entryDateTxt.text = accountInfo.entryDate
        mailTxt.text = accountInfo.mail
        trigramTxt.text = accountInfo.trigram
    }

    private fun connect() {
        progress.visibility = View.VISIBLE
        loginPresenter.login(
                mailEditTxt.editableText.toString(),
                passwordEditTxt.editableText.toString(),
                "$HEADER_BEARER$token")
    }

    companion object {
        val TAG = LoginFragment::class.java.simpleName
        val ACCOUNT_BUNDLE = TAG + ":ACCOUNT_BUNDLE"

        fun newInstance(account: Account?): LoginFragment {
            val fragment = LoginFragment()
            val args = Bundle()
            args.putParcelable(ACCOUNT_BUNDLE, account)
            fragment.arguments = args
            return fragment
        }
    }
}