package fr.axileo.axileoapplication.features.jobs.repositories

import fr.axileo.axileoapplication.features.jobs.entities.Job
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import javax.inject.Singleton

/**
 * Created by BenChaabane on 22/01/2018.
 */

@Singleton
class JobsRepositoryImpl(var retrofit: Retrofit) : JobsRepository {
    val jobsService: JobsRestRepository = retrofit.create(JobsRestRepository::class.java)

    override fun getJobs(): Single<ArrayList<Job>> {
        return jobsService.getJobs("")
                .subscribeOn(Schedulers.io())
    }
}