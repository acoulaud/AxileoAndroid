package fr.axileo.axileoapplication.features.cra.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import fr.axileo.axileoapplication.R
import fr.axileo.axileoapplication.features.cra.entities.CraItem

/**
 * Created by BenChaabane on 30/01/2018.
 */
class CraListAdapter(var craItems: ArrayList<CraItem>) : RecyclerView.Adapter<CraListAdapter.CraListViewHolder>() {

    interface Listener {
        fun onDeleteCra(craItem: CraItem)
    }
    lateinit var clickListener: Listener

    override fun onBindViewHolder(holder: CraListViewHolder, position: Int) {
        if (position < craItems.size && position != RecyclerView.NO_POSITION) {
            val craItem = craItems[position]
            holder.bindViewHolder(craItem)
        }
    }

    override fun getItemCount(): Int {
        return craItems.size
    }

    fun setListener(clickListener: Listener) {
        this.clickListener = clickListener
    }

    fun updateData(craItems: ArrayList<CraItem>) {
        this.craItems = craItems
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CraListViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_cra, parent, false)
        val viewHolder = CraListViewHolder(view)
        viewHolder.craDelete.setOnClickListener({
            val position = viewHolder.adapterPosition
            if (position != RecyclerView.NO_POSITION) {
                val craItem = craItems[position]
                clickListener.onDeleteCra(craItem)
            }
        })
        return viewHolder
    }


    class CraListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @BindView(R.id.cra_month_year) lateinit var monthYearTxt: TextView
        @BindView(R.id.cra_show) lateinit var craShow: ImageView
        @BindView(R.id.cra_edit) lateinit var craEdit: ImageView
        @BindView(R.id.cra_delete) lateinit var craDelete: ImageView

        init {
            ButterKnife.bind(this, itemView)
        }

        fun bindViewHolder(craItem: CraItem) {
            monthYearTxt.text = "${craItem.month} / ${craItem.year}"
        }
    }
}