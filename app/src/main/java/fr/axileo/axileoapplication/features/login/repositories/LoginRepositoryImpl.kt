package fr.axileo.axileoapplication.features.login.repositories

import fr.axileo.axileoapplication.features.login.entities.Account
import fr.axileo.axileoapplication.features.login.entities.User
import fr.axileo.axileoapplication.features.login.entities.UserInfo
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import retrofit2.Response
import retrofit2.Retrofit
import javax.inject.Singleton

/**
 * Created by BenChaabane on 25/01/2018.
 */
@Singleton
class LoginRepositoryImpl(var retrofit: Retrofit) : LoginRepository {

    val loginService: LoginRestRepository = retrofit.create(LoginRestRepository::class.java)

    override fun login(login: String, password: String, token: String): Single<Account> {
        val userInfo = UserInfo(login, password)
        val user = User(userInfo)
        return loginService.login(token, user)
                .subscribeOn(Schedulers.io())
    }

    override fun changePassword(login: String, token: String): Completable {
        val userInfo = UserInfo(login , "")
        val user = User(userInfo)
        return loginService.changePassword(token, user)
                .subscribeOn(Schedulers.io())
    }
}