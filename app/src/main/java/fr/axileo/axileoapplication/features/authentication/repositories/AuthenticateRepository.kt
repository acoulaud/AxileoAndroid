package fr.axileo.axileoapplication.features.authentication.repositories

import fr.axileo.axileoapplication.features.authentication.entities.OauthToken
import io.reactivex.Single

/**
 * Created by BenChaabane on 25/01/2018.
 */
interface AuthenticateRepository {
    fun authenticate() : Single<OauthToken>
}