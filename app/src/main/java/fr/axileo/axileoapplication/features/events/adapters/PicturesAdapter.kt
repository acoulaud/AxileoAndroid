package fr.axileo.axileoapplication.features.events.adapters

import android.net.Uri
import android.support.annotation.NonNull
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import butterknife.BindView
import butterknife.ButterKnife
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.drawee.interfaces.DraweeController
import com.facebook.drawee.view.SimpleDraweeView
import com.facebook.imagepipeline.request.ImageRequest
import com.facebook.imagepipeline.request.ImageRequestBuilder
import fr.axileo.axileoapplication.R

/**
 * Created by BenChaabane on 16/01/2018.
 */
class PicturesAdapter(@NonNull var pictures: ArrayList<String>) : RecyclerView.Adapter<PicturesAdapter.PicturesViewHolder>() {

    override fun onBindViewHolder(holder: PicturesViewHolder, position: Int) {
        if (position < pictures.size && position != RecyclerView.NO_POSITION) {
            val pictureUrl = pictures[position]
            holder.bindViewHolder(pictureUrl)
        }
    }

    override fun getItemCount(): Int {
        return pictures.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PicturesViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_event_picture, parent, false)
        return PicturesViewHolder(view)
    }


    class PicturesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @BindView(R.id.event_picture) lateinit var eventPicture: SimpleDraweeView
        lateinit var imageRequest: ImageRequest
        lateinit var draweeController: DraweeController
        init {
            ButterKnife.bind(this, itemView)
        }

        fun bindViewHolder(@NonNull pictureUrl: String) {
            imageRequest = ImageRequestBuilder.newBuilderWithSource(Uri.parse(pictureUrl))
                    .setProgressiveRenderingEnabled(true)
                    .build()

            draweeController = Fresco.newDraweeControllerBuilder()
                    .setImageRequest(imageRequest)
                    .setOldController(eventPicture.controller)
                    .build()
            eventPicture.controller = draweeController
        }
    }
}