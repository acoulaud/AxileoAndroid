package fr.axileo.axileoapplication.features.members.usecases

import fr.axileo.axileoapplication.features.members.entities.Member
import fr.axileo.axileoapplication.features.members.repositories.MemberRepository
import io.reactivex.Single

/**
 * Created by BenChaabane on 19/01/2018.
 */
class MembersUseCase(var memberRepository: MemberRepository) {

    fun getMembers(): Single<ArrayList<Member>> {
        return memberRepository.getMembers()
    }
}