package fr.axileo.axileoapplication.features.cra.presenters

import android.arch.lifecycle.Lifecycle
import fr.axileo.axileoapplication.features.cra.entities.CraItem
import fr.axileo.axileoapplication.features.login.entities.User

/**
 * Created by BenChaabane on 30/01/2018.
 */
interface CraListContract {

    interface Presenter {
        fun attachPresenter(view: View, lifecycle: Lifecycle)
        fun retrieveAllCra(token: String, user: User)
        fun deleteCra(token: String, craId: String, user: User)
    }

    interface View {
        fun onAllCraRetrieved(craList: ArrayList<CraItem>)
        fun onTokenInvalid()
        fun onAllCraFailure(throwable: Throwable)
        fun onCraDeleted()
        fun onCraDeleteFailure(throwable: Throwable)
    }
}