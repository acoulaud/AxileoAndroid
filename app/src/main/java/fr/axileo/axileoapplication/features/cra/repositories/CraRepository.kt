package fr.axileo.axileoapplication.features.cra.repositories

import fr.axileo.axileoapplication.features.cra.entities.Cra
import fr.axileo.axileoapplication.features.cra.entities.CraItem
import fr.axileo.axileoapplication.features.cra.entities.Hollidays
import fr.axileo.axileoapplication.features.login.entities.User
import io.reactivex.Completable
import io.reactivex.Single

/**
 * Created by BenChaabane on 29/01/2018.
 */
interface CraRepository {
    fun addCra(token: String, cra: Cra) : Completable
    fun getHolidays(token: String, year: String) : Single<Hollidays>
    fun retrieveAllCra(token: String, user: User) : Single<ArrayList<CraItem>>
    fun deleteCra(token: String, craId: String, user: User) : Completable
}