package fr.axileo.axileoapplication.features.login.usecase

import fr.axileo.axileoapplication.features.login.repositories.LoginRepository

/**
 * Created by BenChaabane on 25/01/2018.
 */
class LoginUseCase(var loginRepository: LoginRepository) {

    fun login(login: String, password: String, token: String) = loginRepository.login(login, password, token)
    fun changePassword(login: String, token: String) = loginRepository.changePassword(login, token)
}