package fr.axileo.axileoapplication.features.contacts.ui.fragments

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.TabItem
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import butterknife.BindView
import butterknife.ButterKnife
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import fr.axileo.axileoapplication.R
import fr.axileo.axileoapplication.events.UpdateToolbarTitleEvent
import fr.axileo.axileoapplication.launchers.AxileoApplication
import org.greenrobot.eventbus.EventBus
import javax.inject.Inject

/**
 * Created by BenChaabane on 18/01/2018.
 */
class ContactFragment : Fragment(), OnMapReadyCallback, TabLayout.OnTabSelectedListener {

    @BindView(R.id.contact_tabLayout) lateinit var tabLayout: TabLayout
    @BindView(R.id.mapView) lateinit var mapView: MapView
    @BindView(R.id.paris_layout_information) lateinit var parisInformationLayout: LinearLayout
    @BindView(R.id.toulouse_layout_information) lateinit var toulouseInformationLayout: LinearLayout
    @BindView(R.id.fab_contact_mail) lateinit var mailFab: FloatingActionButton
    @BindView(R.id.fab_contact_phone) lateinit var phoneFab: FloatingActionButton

    @Inject lateinit var eventBus: EventBus
    lateinit var map: GoogleMap

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AxileoApplication.getApplication(context).getFragmentsComponent().resolveDependencies(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_contact, container, false)
        ButterKnife.bind(this, view)
        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tabLayout.addOnTabSelectedListener(this)
        mapView.onCreate(savedInstanceState)
        mapView.getMapAsync(this)
        mailFab.setOnClickListener({sendMailToAxileo()})
        phoneFab.setOnClickListener({callAxileo()})
    }


    fun callAxileo() {
        val phoneNumber = if (tabLayout.selectedTabPosition == 0) getString(R.string.phone_description_paris)
        else getString(R.string.phone_description_toulouse)
        Log.e("phone number", phoneNumber)
        val intent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phoneNumber, null))
        if (intent.resolveActivity(activity.packageManager) != null) {
            startActivity(Intent.createChooser(intent, getString(R.string.share_chooser)))
        }

    }


    fun sendMailToAxileo() {
        val intent = Intent(Intent.ACTION_SENDTO)
        with (intent) {
            type = "text/plain"
            data = Uri.parse("mailto:")
            putExtra(Intent.EXTRA_EMAIL, arrayOf("drh@axileo.fr"))
            putExtra(Intent.EXTRA_SUBJECT, "Contact depuis Axileo Android app")
        }
        if (intent.resolveActivity(activity.packageManager) != null) {
            startActivity(Intent.createChooser(intent, getString(R.string.share_chooser)))
        }

    }

    override fun onResume() {
        super.onResume()
        eventBus.post(UpdateToolbarTitleEvent(getString(R.string.contact)))
        mapView.onResume()
    }

    override fun onStop() {
        super.onStop()
        mapView.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        mapView.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView.onLowMemory()
    }

    @SuppressLint("MissingPermission")
    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        map.isMyLocationEnabled = false
        map.uiSettings.isMyLocationButtonEnabled = false
        MapsInitializer.initialize(context)
        if (tabLayout.selectedTabPosition == 0) addMarker(latLngParis) else addMarker(latLngToulouse)
    }

    override fun onTabReselected(tab: TabLayout.Tab?) {
    }

    override fun onTabUnselected(tab: TabLayout.Tab?) {
    }

    override fun onTabSelected(tab: TabLayout.Tab?) {
        when (tab?.position) {
            0 -> {
                parisInformationLayout.visibility = View.VISIBLE
                toulouseInformationLayout.visibility = View.GONE
                addMarker(latLngParis)
            }
            else -> {
                parisInformationLayout.visibility = View.GONE
                toulouseInformationLayout.visibility = View.VISIBLE
                addMarker(latLngToulouse)
            }
        }
    }

    fun addMarker(latLng: LatLng) {
        map.clear()
        map.addMarker(MarkerOptions().position(latLng))
        val cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 17f)
        map.animateCamera(cameraUpdate)
    }


    companion object {
        val TAG = ContactFragment::class.java.simpleName
        private val PARIS_LATITUDE = 48.894315
        private val PARIS_LONGITUDE = 2.298640
        private val TOULOUSE_LATITUDE = 43.612688
        private val TOULOUSE_LONGITUDE = 1.429772
        private val latLngParis = LatLng(PARIS_LATITUDE, PARIS_LONGITUDE)
        private val latLngToulouse = LatLng(TOULOUSE_LATITUDE, TOULOUSE_LONGITUDE)

        fun newInstance() = ContactFragment()
    }
}