package fr.axileo.axileoapplication.features.authentication.repositories

import fr.axileo.axileoapplication.features.authentication.constants.AuthenticationConstants.Companion.CLIENT_ID
import fr.axileo.axileoapplication.features.authentication.constants.AuthenticationConstants.Companion.CLIENT_SECRET
import fr.axileo.axileoapplication.features.authentication.constants.AuthenticationConstants.Companion.GRANT_TYPE
import fr.axileo.axileoapplication.features.authentication.constants.AuthenticationConstants.Companion.USER_NAME
import fr.axileo.axileoapplication.features.authentication.constants.AuthenticationConstants.Companion.USER_PASSWORD
import fr.axileo.axileoapplication.features.authentication.entities.OauthToken
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import javax.inject.Singleton

/**
 * Created by BenChaabane on 25/01/2018.
 */

@Singleton
class AuthenticateRepositoryImpl(var retrofit: Retrofit) : AuthenticateRepository {
    private val authenticateService: AuthenticationRestRepository = retrofit.create(AuthenticationRestRepository::class.java)

    override fun authenticate(): Single<OauthToken> {
        return authenticateService.authenticate(GRANT_TYPE, CLIENT_ID, CLIENT_SECRET,
                USER_NAME, USER_PASSWORD)
                .subscribeOn(Schedulers.io())
    }
}