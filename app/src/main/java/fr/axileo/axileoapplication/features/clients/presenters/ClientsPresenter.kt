package fr.axileo.axileoapplication.features.clients.presenters

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.OnLifecycleEvent
import android.content.Context
import fr.axileo.axileoapplication.features.clients.entities.Client
import fr.axileo.axileoapplication.features.clients.usecases.ClientsUseCase
import fr.axileo.axileoapplication.launchers.AxileoApplication
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import javax.inject.Inject

/**
 * Created by BenChaabane on 22/01/2018.
 */
class ClientsPresenter(var context: Context) : ClientsContract.Presenter, LifecycleObserver {

    @Inject lateinit var clientsUseCase: ClientsUseCase
    private val compositeDisposable: CompositeDisposable
    private lateinit var view: ClientsContract.View

    init {
        AxileoApplication.getApplication(context).getPresentersComponent().resolveDependencies(this)
        compositeDisposable = CompositeDisposable()
    }

    override fun attachPresenter(view: ClientsContract.View, lifecycle: Lifecycle) {
        this.view = view
        lifecycle.addObserver(this)
    }

    override fun getClients() {
        clientsUseCase.getClients()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : SingleObserver<ArrayList<Client>> {
                    override fun onSuccess(t: ArrayList<Client>) {
                        view.onClientsRetrieved(t)
                    }

                    override fun onError(e: Throwable) {
                        view.onClientsError(e)
                    }

                    override fun onSubscribe(d: Disposable) {
                       if (!d.isDisposed) compositeDisposable.add(d)
                    }

                })
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun detachPresenter() {
        compositeDisposable.clear()
    }
}