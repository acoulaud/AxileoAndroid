package fr.axileo.axileoapplication.features.members.repositories

import fr.axileo.axileoapplication.features.members.entities.Member
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by BenChaabane on 19/01/2018.
 */
interface MemberRestRepository {
    @GET("application_api")
    fun getMembers(@Query("trombi") trombi: String): Single<ArrayList<Member>>
}