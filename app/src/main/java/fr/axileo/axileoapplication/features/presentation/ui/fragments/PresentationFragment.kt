package fr.axileo.axileoapplication.features.presentation.ui.fragments

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.BindView
import butterknife.ButterKnife
import fr.axileo.axileoapplication.R
import fr.axileo.axileoapplication.events.UpdateToolbarTitleEvent
import fr.axileo.axileoapplication.launchers.AxileoApplication
import org.greenrobot.eventbus.EventBus
import javax.inject.Inject

/**
 * Created by BenChaabane on 17/01/2018.
 */
class PresentationFragment : Fragment() {

    @BindView(R.id.presentation_share_fab) lateinit var shareFab: FloatingActionButton

    @Inject lateinit var eventBus: EventBus

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AxileoApplication.getApplication(context).getFragmentsComponent().resolveDependencies(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_presentation, container, false)
        ButterKnife.bind(this, view)
        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        shareFab.setOnClickListener({ sharePresentation() })
    }

    override fun onResume() {
        super.onResume()
        eventBus.post(UpdateToolbarTitleEvent(getString(R.string.presentation)))
    }

    fun sharePresentation() {
        val intent = Intent(Intent.ACTION_SEND)
        with (intent) {
            type = "text/plain"
            putExtra(Intent.EXTRA_TEXT, getString(R.string.axileo_website))
        }
        if (intent.resolveActivity(activity.packageManager) != null) {
            startActivity(Intent.createChooser(intent, getString(R.string.share_chooser)))
        }

    }

    companion object {
        val TAG = PresentationFragment::class.java.simpleName
        fun newInstance() = PresentationFragment()
    }
}