package fr.axileo.axileoapplication.features.cra.entities

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created by BenChaabane on 29/01/2018.
 */
data class CraDay(@SerializedName("numJour") var numDay: String,
                  @SerializedName("travaille") var working: String,
                  @SerializedName("projet") var project: String = "Siége",
                  @SerializedName("r") var r: Boolean = false,
                  @SerializedName("sp") var sp: Boolean = false,
                  @SerializedName("pc") var pc: Boolean = false,
                  @SerializedName("es") var es: Boolean = false,
                  @SerializedName("vm") var vm: Boolean = false,
                  @SerializedName("f") var f: Boolean = false,
                  @SerializedName("siteClient") var siteClient: Boolean = true,
                  @SerializedName("siteAutres") var siteOther: String = "",
                  @SerializedName("cp") var cp: String = "0",
                  @SerializedName("jrtt") var jrtt: String = "0",
                  @SerializedName("ce") var ce: String = "0",
                  @SerializedName("am") var am: String = "0",
                  @SerializedName("autreConges") var otherAbsence: String = "0",
                  @SerializedName("autreCongesMotif") var otherabsenceArg: String = "") : Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readByte() != 0.toByte(),
            parcel.readByte() != 0.toByte(),
            parcel.readByte() != 0.toByte(),
            parcel.readByte() != 0.toByte(),
            parcel.readByte() != 0.toByte(),
            parcel.readByte() != 0.toByte(),
            parcel.readByte() != 0.toByte(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(numDay)
        parcel.writeString(working)
        parcel.writeString(project)
        parcel.writeByte(if (r) 1 else 0)
        parcel.writeByte(if (sp) 1 else 0)
        parcel.writeByte(if (pc) 1 else 0)
        parcel.writeByte(if (es) 1 else 0)
        parcel.writeByte(if (vm) 1 else 0)
        parcel.writeByte(if (f) 1 else 0)
        parcel.writeByte(if (siteClient) 1 else 0)
        parcel.writeString(siteOther)
        parcel.writeString(cp)
        parcel.writeString(jrtt)
        parcel.writeString(ce)
        parcel.writeString(am)
        parcel.writeString(otherAbsence)
        parcel.writeString(otherabsenceArg)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CraDay> {
        override fun createFromParcel(parcel: Parcel): CraDay {
            return CraDay(parcel)
        }

        override fun newArray(size: Int): Array<CraDay?> {
            return arrayOfNulls(size)
        }
    }

    override fun equals(other: Any?): Boolean {
        if (other == null) return false
        return (other as CraDay).numDay == numDay
    }
}