package fr.axileo.axileoapplication.features.events.adapters

import android.net.Uri
import android.support.annotation.NonNull
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.drawee.interfaces.DraweeController
import com.facebook.drawee.view.SimpleDraweeView
import com.facebook.imagepipeline.request.ImageRequest
import com.facebook.imagepipeline.request.ImageRequestBuilder
import fr.axileo.axileoapplication.R
import fr.axileo.axileoapplication.features.events.entities.Event
import java.net.URI
import java.util.*

/**
 * Created by BenChaabane on 16/01/2018.
 */
class EventsAdapter(var events: ArrayList<Event>) : RecyclerView.Adapter<EventsAdapter.EventViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_event, parent, false)
        return EventViewHolder(view)
    }

    override fun onBindViewHolder(holder: EventViewHolder, position: Int) {
        if (position < events.size && position != RecyclerView.NO_POSITION) {
            val event = events[position]
            holder.bindViewHolder(event)
        }
    }

    override fun getItemCount(): Int {
        return events.size
    }

    class EventViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        @BindView(R.id.event_cover_picture) lateinit var cover: SimpleDraweeView
        @BindView(R.id.event_title) lateinit var title: TextView
        @BindView(R.id.event_date) lateinit var date: TextView
        @BindView(R.id.event_description) lateinit var description: TextView
        @BindView(R.id.event_address) lateinit var address: TextView
        @BindView(R.id.event_pictures_recycler) lateinit var picturesRecycler: RecyclerView

        lateinit var picturesAdapter: PicturesAdapter
        lateinit var imageRequest: ImageRequest
        lateinit var draweeController: DraweeController

        init {
            ButterKnife.bind(this, itemView)
        }

        fun bindViewHolder(@NonNull event: Event) {
            imageRequest = ImageRequestBuilder.newBuilderWithSource(Uri.parse(event.imageCover))
                    .setProgressiveRenderingEnabled(true)
                    .build()

            draweeController = Fresco.newDraweeControllerBuilder()
                    .setImageRequest(imageRequest)
                    .setOldController(cover.controller)
                    .build()
            cover.controller = draweeController
            title.text = event.title
            date.text = event.eventDate
            address.text = event.eventAddress
            if (!TextUtils.isEmpty(event.eventDescription)) description.text = event.eventDescription
            else description.visibility = View.GONE

            val pictures = event.images
            if (pictures.size > 0) {
                picturesAdapter = PicturesAdapter(pictures)
                picturesRecycler.hasFixedSize()
                picturesRecycler.adapter = picturesAdapter
            }
        }
    }
}