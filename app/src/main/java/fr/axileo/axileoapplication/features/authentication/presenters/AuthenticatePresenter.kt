package fr.axileo.axileoapplication.features.authentication.presenters

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.OnLifecycleEvent
import android.content.Context
import fr.axileo.axileoapplication.features.authentication.entities.OauthToken
import fr.axileo.axileoapplication.features.authentication.usecases.AuthenticateUseCase
import fr.axileo.axileoapplication.launchers.AxileoApplication
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import javax.inject.Inject

/**
 * Created by BenChaabane on 25/01/2018.
 */
class AuthenticatePresenter(var context: Context) : AuthenticateContract.Presenter, LifecycleObserver {

    @Inject lateinit var authenticateUseCase: AuthenticateUseCase
    lateinit var view: AuthenticateContract.View
    private val compositeDisposable = CompositeDisposable()

    init {
        AxileoApplication.getApplication(context).getPresentersComponent().resolveDependencies(this)
    }

    override fun attachPresenter(view: AuthenticateContract.View, lifecycle: Lifecycle) {
        this.view = view
        lifecycle.addObserver(this)
    }

    override fun authenticate() {
        authenticateUseCase.authenticate()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : SingleObserver<OauthToken> {
                    override fun onSuccess(t: OauthToken) {
                        view.onAuthenticationSuccess(t)
                    }

                    override fun onSubscribe(d: Disposable) {
                        if (!d.isDisposed) compositeDisposable.add(d)
                    }

                    override fun onError(e: Throwable) {
                        view.onAuthenticationFailed(e)
                    }

                })
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun detachPresenter() {
        compositeDisposable.clear()
    }
}