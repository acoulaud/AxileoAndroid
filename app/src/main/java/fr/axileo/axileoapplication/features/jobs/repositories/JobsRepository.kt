package fr.axileo.axileoapplication.features.jobs.repositories

import fr.axileo.axileoapplication.features.jobs.entities.Job
import io.reactivex.Single

/**
 * Created by BenChaabane on 22/01/2018.
 */
interface JobsRepository {
    fun getJobs() : Single<ArrayList<Job>>
}