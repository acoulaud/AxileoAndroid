package fr.axileo.axileoapplication.features.authentication.usecases

import fr.axileo.axileoapplication.features.authentication.repositories.AuthenticateRepository

/**
 * Created by BenChaabane on 25/01/2018.
 */

class AuthenticateUseCase(var authenticateRepository: AuthenticateRepository) {
    fun authenticate() = authenticateRepository.authenticate()
}