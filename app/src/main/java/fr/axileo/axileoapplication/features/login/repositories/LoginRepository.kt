package fr.axileo.axileoapplication.features.login.repositories

import fr.axileo.axileoapplication.features.login.entities.Account
import io.reactivex.Completable
import io.reactivex.Single
import retrofit2.Response

/**
 * Created by BenChaabane on 25/01/2018.
 */
interface LoginRepository {

    fun login(login: String, password: String, token: String) : Single<Account>
    fun changePassword(login: String, token: String) : Completable
}