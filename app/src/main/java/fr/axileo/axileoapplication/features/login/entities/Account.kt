package fr.axileo.axileoapplication.features.login.entities

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created by BenChaabane on 25/01/2018.
 */
data class Account(@SerializedName("user") var accountInfo: AccountInfo) : Parcelable {
    constructor(parcel: Parcel) : this( accountInfo = parcel.readParcelable(AccountInfo::class.java.classLoader))

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(accountInfo, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Account> {
        override fun createFromParcel(parcel: Parcel): Account {
            return Account(parcel)
        }

        override fun newArray(size: Int): Array<Account?> {
            return arrayOfNulls(size)
        }
    }
}