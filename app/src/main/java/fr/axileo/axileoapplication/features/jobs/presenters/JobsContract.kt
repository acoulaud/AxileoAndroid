package fr.axileo.axileoapplication.features.jobs.presenters

import android.arch.lifecycle.Lifecycle
import fr.axileo.axileoapplication.features.jobs.entities.Job

/**
 * Created by BenChaabane on 22/01/2018.
 */
interface JobsContract {

    interface Presenter {
        fun getJobs()
        fun attachPresenter(view: JobsContract.View, lifecycle: Lifecycle)
    }

    interface View {
        fun onJobsRetrieved(jobs: ArrayList<Job>)
        fun onJobError(throwable: Throwable)
    }
}