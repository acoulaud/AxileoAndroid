package fr.axileo.axileoapplication.features.authentication.presenters

import android.arch.lifecycle.Lifecycle
import fr.axileo.axileoapplication.features.authentication.entities.OauthToken

/**
 * Created by BenChaabane on 25/01/2018.
 */
interface AuthenticateContract {

    interface Presenter {
        fun attachPresenter(view: View, lifecycle: Lifecycle)
        fun authenticate()
    }

    interface View {
        fun onAuthenticationSuccess(oauthToken: OauthToken)
        fun onAuthenticationFailed(throwable: Throwable)
    }
}