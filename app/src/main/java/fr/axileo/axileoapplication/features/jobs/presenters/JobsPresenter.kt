package fr.axileo.axileoapplication.features.jobs.presenters

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.OnLifecycleEvent
import android.content.Context
import fr.axileo.axileoapplication.features.jobs.entities.Job
import fr.axileo.axileoapplication.features.jobs.usecases.JobsUseCase
import fr.axileo.axileoapplication.launchers.AxileoApplication
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import javax.inject.Inject

/**
 * Created by BenChaabane on 22/01/2018.
 */
class JobsPresenter(var context: Context) : JobsContract.Presenter, LifecycleObserver {

    @Inject lateinit var jobsUseCase: JobsUseCase
    lateinit var view: JobsContract.View
    private var compositeDisposable: CompositeDisposable

    init {
        AxileoApplication.getApplication(context).getPresentersComponent().resolveDependencies(this)
        compositeDisposable = CompositeDisposable()
    }

    override fun getJobs() {
        jobsUseCase.getJobs()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : SingleObserver<ArrayList<Job>> {
                    override fun onSubscribe(d: Disposable) {
                        if (!d.isDisposed) compositeDisposable.add(d)
                    }

                    override fun onError(e: Throwable) {
                        view.onJobError(e)
                    }

                    override fun onSuccess(t: ArrayList<Job>) {
                        view.onJobsRetrieved(t)
                    }

                })
    }

    override fun attachPresenter(view: JobsContract.View, lifecycle: Lifecycle) {
        this.view = view
        lifecycle.addObserver(this)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun detachPresenter() {
        compositeDisposable.clear()
    }
}