package fr.axileo.axileoapplication.features.events.entities

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created by BenChaabane on 15/01/2018.
 */

class Event() : Parcelable {

    @SerializedName("title") lateinit var title: String
    @SerializedName("event_date") lateinit var eventDate: String
    @SerializedName("event_description") lateinit var eventDescription: String
    @SerializedName("event_address") lateinit var eventAddress: String
    @SerializedName("image_cover") lateinit var imageCover: String
    @SerializedName("url_images") lateinit var images: ArrayList<String>
    @SerializedName("fb_album") lateinit var fbAlbum: String

    constructor(parcel: Parcel) : this() {
        title = parcel.readString()
        eventDate = parcel.readString()
        eventDescription = parcel.readString()
        eventAddress = parcel.readString()
        imageCover = parcel.readString()
        images = parcel.readArrayList(String::class.java.classLoader) as ArrayList<String>
        fbAlbum = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(title)
        parcel.writeString(eventDate)
        parcel.writeString(eventDescription)
        parcel.writeString(eventAddress)
        parcel.writeString(imageCover)
        parcel.writeList(images)
        parcel.writeString(fbAlbum)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Event> {
        override fun createFromParcel(parcel: Parcel): Event {
            return Event(parcel)
        }

        override fun newArray(size: Int): Array<Event?> {
            return arrayOfNulls(size)
        }
    }
}