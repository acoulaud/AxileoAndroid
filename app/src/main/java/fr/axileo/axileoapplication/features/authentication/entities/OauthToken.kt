package fr.axileo.axileoapplication.features.authentication.entities

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created by BenChaabane on 25/01/2018.
 */
data class OauthToken(@SerializedName("access_token") var accessToken: String,
                      @SerializedName("expires_in") var expiresIn: Int,
                      @SerializedName("refresh_token") var refreshToken: String,
                      @SerializedName("token_type") var tokenType: String) : Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readInt(),
            parcel.readString(),
            parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(accessToken)
        parcel.writeInt(expiresIn)
        parcel.writeString(refreshToken)
        parcel.writeString(tokenType)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<OauthToken> {
        override fun createFromParcel(parcel: Parcel): OauthToken {
            return OauthToken(parcel)
        }

        override fun newArray(size: Int): Array<OauthToken?> {
            return arrayOfNulls(size)
        }
    }
}