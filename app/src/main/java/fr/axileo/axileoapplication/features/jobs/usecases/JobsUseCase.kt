package fr.axileo.axileoapplication.features.jobs.usecases

import fr.axileo.axileoapplication.features.jobs.repositories.JobsRepository

/**
 * Created by BenChaabane on 22/01/2018.
 */
class JobsUseCase(var jobsRepository: JobsRepository) {

    fun getJobs() = jobsRepository.getJobs()
}