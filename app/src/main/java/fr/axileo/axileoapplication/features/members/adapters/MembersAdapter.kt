package fr.axileo.axileoapplication.features.members.adapters

import android.content.Intent
import android.net.Uri
import android.support.annotation.NonNull
import android.support.design.widget.FloatingActionButton
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.drawee.interfaces.DraweeController
import com.facebook.drawee.view.SimpleDraweeView
import com.facebook.imagepipeline.request.ImageRequest
import com.facebook.imagepipeline.request.ImageRequestBuilder
import fr.axileo.axileoapplication.R
import fr.axileo.axileoapplication.features.members.entities.Member

/**
 * Created by BenChaabane on 19/01/2018.
 */
class MembersAdapter(@NonNull private var members: ArrayList<Member>) : RecyclerView.Adapter<MembersAdapter.MemberViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MemberViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_member, parent, false)
        return MemberViewHolder(view)
    }

    override fun onBindViewHolder(holder: MemberViewHolder, position: Int) {
        if (position < members.size && position != RecyclerView.NO_POSITION) {
            val member = members[position]
            holder.bindViewHolder(member)
        }
    }

    override fun getItemCount(): Int {
        return members.size
    }

    fun updateData(members: ArrayList<Member>) {
        this.members = members
        notifyDataSetChanged()
    }

    class MemberViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @BindView(R.id.member_picture) lateinit var memberPicture: SimpleDraweeView
        @BindView(R.id.member_name) lateinit var memberName: TextView
        @BindView(R.id.member_job) lateinit var memberJob: TextView
        @BindView(R.id.member_contact_fab) lateinit var memberContactFab: FloatingActionButton
        lateinit var imageRequest: ImageRequest
        lateinit var draweeController: DraweeController

        init {
            ButterKnife.bind(this, itemView)
        }

        fun bindViewHolder(member: Member) {
            imageRequest = ImageRequestBuilder.newBuilderWithSource(Uri.parse(member.picture))
                    .setProgressiveRenderingEnabled(true)
                    .build()

            draweeController = Fresco.newDraweeControllerBuilder()
                    .setImageRequest(imageRequest)
                    .setOldController(memberPicture.controller)
                    .build()
            memberPicture.controller = draweeController

            memberName.text = member.name
            memberJob.text = member.job
            memberContactFab.setOnClickListener({sendMailTo("drh@axileo.fr")})
        }

        fun sendMailTo(mail: String) {
            val intent = Intent(Intent.ACTION_SENDTO)
            with (intent) {
                type = "text/plain"
                data = Uri.parse("mailto:")
                putExtra(Intent.EXTRA_EMAIL, arrayOf(mail))
                putExtra(Intent.EXTRA_SUBJECT, "Contact depuis Axileo Android app")
            }
            if (intent.resolveActivity(itemView.context.packageManager) != null) {
                itemView.context.startActivity(Intent.createChooser(intent, itemView.context
                        .getString(R.string.share_chooser)))
            }
        }
    }
}