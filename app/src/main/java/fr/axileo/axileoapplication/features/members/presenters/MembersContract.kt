package fr.axileo.axileoapplication.features.members.presenters

import android.arch.lifecycle.Lifecycle
import fr.axileo.axileoapplication.features.members.entities.Member

/**
 * Created by BenChaabane on 19/01/2018.
 */
interface MembersContract {

    interface Presenter {
        fun attachPresenter(view: MembersContract.View, lifecycle: Lifecycle)
        fun getMembers()
    }

    interface View {
        fun onMembersRetrieved(members: ArrayList<Member>)
        fun onMembersError(throwable: Throwable)
    }
}