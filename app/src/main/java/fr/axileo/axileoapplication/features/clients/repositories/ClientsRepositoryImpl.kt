package fr.axileo.axileoapplication.features.clients.repositories

import fr.axileo.axileoapplication.features.clients.entities.Client
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import javax.inject.Singleton

/**
 * Created by BenChaabane on 22/01/2018.
 */
@Singleton
class ClientsRepositoryImpl(var retrofit: Retrofit) : ClientsRepository {

    private val clientsService: ClientsRestRepository = retrofit.create(ClientsRestRepository::class.java)

    override fun getClients(): Single<ArrayList<Client>> {
        return clientsService.getClients("")
                .subscribeOn(Schedulers.io())
    }
}