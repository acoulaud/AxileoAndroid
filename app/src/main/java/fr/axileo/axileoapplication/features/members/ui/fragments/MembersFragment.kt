package fr.axileo.axileoapplication.features.members.ui.fragments

import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import butterknife.BindView
import butterknife.ButterKnife
import fr.axileo.axileoapplication.R
import fr.axileo.axileoapplication.events.UpdateToolbarTitleEvent
import fr.axileo.axileoapplication.features.members.adapters.MembersAdapter
import fr.axileo.axileoapplication.features.members.entities.Member
import fr.axileo.axileoapplication.features.members.presenters.MembersContract
import fr.axileo.axileoapplication.features.members.presenters.MembersPresenter
import fr.axileo.axileoapplication.launchers.AxileoApplication
import org.greenrobot.eventbus.EventBus
import javax.inject.Inject


/**
 * Created by BenChaabane on 19/01/2018.
 */
class MembersFragment : Fragment(), MembersContract.View {
    @BindView(R.id.members_recyclerView) lateinit var membersRecyclerView: RecyclerView
    @Inject lateinit var eventBus: EventBus
    private lateinit var membersAdapter: MembersAdapter
    private lateinit var members: ArrayList<Member>
    private lateinit var presenter: MembersPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AxileoApplication.getApplication(context).getFragmentsComponent().resolveDependencies(this)
        members = ArrayList()
        if (savedInstanceState != null) {
            members = savedInstanceState.getParcelableArrayList(BUNDLE_MEMBERS)
        }
        membersAdapter = MembersAdapter(members)
        presenter = MembersPresenter(context)
        presenter.attachPresenter(this, lifecycle)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_members, container, false)
        ButterKnife.bind(this, view)
        with(membersRecyclerView) {
            hasFixedSize()
            adapter = membersAdapter
        }
        if (members.size == 0) presenter.getMembers()
        return view
    }

    override fun onResume() {
        super.onResume()
        eventBus.post(UpdateToolbarTitleEvent(getString(R.string.members)))
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putParcelableArrayList(BUNDLE_MEMBERS, members)
    }

    override fun onMembersRetrieved(members: ArrayList<Member>) {
        membersAdapter.updateData(members)
    }

    override fun onMembersError(throwable: Throwable) {
        if (isAdded) {
            AlertDialog.Builder(context)
                    .setMessage(R.string.splash_warning_text)
                    .setOnDismissListener({activity.supportFragmentManager.popBackStackImmediate()})
                    .create()
                    .show()
        }
    }

    companion object {
        val TAG = MembersFragment::class.java.simpleName
        val BUNDLE_MEMBERS = TAG + ":BUNDLE_MEMBERS"

        fun newInstance() = MembersFragment()
    }
}