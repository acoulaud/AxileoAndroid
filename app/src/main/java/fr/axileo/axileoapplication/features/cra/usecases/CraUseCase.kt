package fr.axileo.axileoapplication.features.cra.usecases

import fr.axileo.axileoapplication.features.cra.entities.Cra
import fr.axileo.axileoapplication.features.cra.repositories.CraRepository
import fr.axileo.axileoapplication.features.login.entities.User

/**
 * Created by BenChaabane on 29/01/2018.
 */
class CraUseCase(var craRepository: CraRepository) {
    fun addCra(token: String, cra: Cra) = craRepository.addCra(token, cra)
    fun getHolidays(token: String, year: String) = craRepository.getHolidays(token, year)
    fun retrieveAllCra(token: String, user: User) = craRepository.retrieveAllCra(token, user)
    fun deleteCra(token: String, craId: String, user: User) = craRepository.deleteCra(token, craId, user)
}