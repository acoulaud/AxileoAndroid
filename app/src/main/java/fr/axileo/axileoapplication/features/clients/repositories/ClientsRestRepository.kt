package fr.axileo.axileoapplication.features.clients.repositories

import fr.axileo.axileoapplication.features.clients.entities.Client
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by BenChaabane on 22/01/2018.
 */
interface ClientsRestRepository {

    @GET("application_api")
    fun getClients(@Query("clients") clients: String) : Single<ArrayList<Client>>
}