package fr.axileo.axileoapplication.features.clients.repositories

import fr.axileo.axileoapplication.features.clients.entities.Client
import io.reactivex.Single

/**
 * Created by BenChaabane on 22/01/2018.
 */
interface ClientsRepository {
    fun getClients() : Single<ArrayList<Client>>
}