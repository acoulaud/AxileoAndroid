package fr.axileo.axileoapplication.features.login.entities

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created by BenChaabane on 25/01/2018.
 */
data class AccountInfo(@SerializedName("nom") var name: String,
                       @SerializedName("prenom") var lastName: String,
                       @SerializedName("date_entree") var entryDate: String,
                       @SerializedName("fonction") var position: String,
                       @SerializedName("emailPro") var mail: String,
                       @SerializedName("trigramme") var trigram: String) : Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(lastName)
        parcel.writeString(entryDate)
        parcel.writeString(position)
        parcel.writeString(mail)
        parcel.writeString(trigram)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<AccountInfo> {
        override fun createFromParcel(parcel: Parcel): AccountInfo {
            return AccountInfo(parcel)
        }

        override fun newArray(size: Int): Array<AccountInfo?> {
            return arrayOfNulls(size)
        }
    }
}