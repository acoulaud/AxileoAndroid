package fr.axileo.axileoapplication.features.login.presenters

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.OnLifecycleEvent
import android.content.Context
import fr.axileo.axileoapplication.features.login.entities.Account
import fr.axileo.axileoapplication.features.login.usecase.LoginUseCase
import fr.axileo.axileoapplication.launchers.AxileoApplication
import io.reactivex.CompletableObserver
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import retrofit2.HttpException
import java.net.HttpURLConnection
import javax.inject.Inject

/**
 * Created by BenChaabane on 25/01/2018.
 */
class LoginPresenter(var context: Context) : LoginContract.Presenter, LifecycleObserver {
    @Inject lateinit var loginUseCase: LoginUseCase
    lateinit var view: LoginContract.View
    val compositeDisposable = CompositeDisposable()

    init {
        AxileoApplication.getApplication(context).getPresentersComponent().resolveDependencies(this)
    }

    override fun attachPresenter(view: LoginContract.View, lifecycle: Lifecycle) {
        this.view = view
        lifecycle.addObserver(this)
    }

    override fun login(login: String, password: String, token: String) {
        loginUseCase.login(login, password, token)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : SingleObserver<Account> {
                    override fun onSubscribe(d: Disposable) {
                        if (!d.isDisposed) compositeDisposable.add(d)
                    }

                    override fun onError(e: Throwable) {
                        if (e is HttpException) {
                            when (e.code()) {
                                HttpURLConnection.HTTP_UNAUTHORIZED -> { view.onTokenInvalid() }
                                else -> view.onLoginFailed()
                            }
                        } else view.onLoginFailed()
                    }

                    override fun onSuccess(response: Account) {
                       view.onUserLogged(response)
                    }

                })
    }

    override fun changePassword(login: String, token: String) {
        loginUseCase.changePassword(login, token)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : CompletableObserver {
                    override fun onComplete() {
                        view.onPasswordChangeRequestSucceded()
                    }

                    override fun onSubscribe(d: Disposable) {
                        if (!d.isDisposed) compositeDisposable.add(d)
                    }

                    override fun onError(e: Throwable) {
                        if (e is HttpException) {
                            when (e.code()) {
                                HttpURLConnection.HTTP_UNAUTHORIZED -> { view.onTokenInvalid() }
                                else -> view.onPasswordChangeRequestFailed()
                            }
                        } else view.onPasswordChangeRequestFailed()
                    }

                })
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun detachPresenter() {
        compositeDisposable.clear()
    }
}