package fr.axileo.axileoapplication.features.jobs.repositories

import fr.axileo.axileoapplication.features.jobs.entities.Job
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by BenChaabane on 22/01/2018.
 */
interface JobsRestRepository {
    @GET("application_api")
    fun getJobs(@Query("careers") careers: String) : Single<ArrayList<Job>>
}