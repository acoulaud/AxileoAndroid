package fr.axileo.axileoapplication.features.login.entities

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created by BenChaabane on 25/01/2018.
 */
data class User(@SerializedName("user") var userInfo: UserInfo) : Parcelable {
    constructor(parcel: Parcel) : this(userInfo = parcel.readParcelable(UserInfo::class.java.classLoader))

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(userInfo, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<User> {
        override fun createFromParcel(parcel: Parcel): User {
            return User(parcel)
        }

        override fun newArray(size: Int): Array<User?> {
            return arrayOfNulls(size)
        }
    }
}