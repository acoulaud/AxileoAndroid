package fr.axileo.axileoapplication.features.events.repositories

import fr.axileo.axileoapplication.features.events.entities.Event
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by BenChaabane on 15/01/2018.
 */

interface EventsRestRepository {
    @GET("application_api")
    fun getEvents(@Query("events") events:String): Single<ArrayList<Event>>
}