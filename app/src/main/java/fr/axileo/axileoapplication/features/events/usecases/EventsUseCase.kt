package fr.axileo.axileoapplication.features.events.usecases

import fr.axileo.axileoapplication.features.events.repositories.EventsRepository
import io.reactivex.annotations.NonNull

/**
 * Created by BenChaabane on 15/01/2018.
 */

class EventsUseCase {

    var eventsRepository: EventsRepository

    constructor(@NonNull eventsRepository: EventsRepository) {
        this.eventsRepository = eventsRepository
    }

    fun getEvents() = eventsRepository.getEvents()
}