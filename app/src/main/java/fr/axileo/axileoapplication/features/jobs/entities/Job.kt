package fr.axileo.axileoapplication.features.jobs.entities

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created by BenChaabane on 22/01/2018.
 */
data class Job(@SerializedName("title") var title: String,
               @SerializedName("location") var location: String,
               @SerializedName("contract_type") var type: String,
               @SerializedName("description") var description: String,
               @SerializedName("posted_date") var date: String,
               @SerializedName("link") var link: String
               ) : Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(title)
        parcel.writeString(location)
        parcel.writeString(type)
        parcel.writeString(description)
        parcel.writeString(date)
        parcel.writeString(link)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Job> {
        override fun createFromParcel(parcel: Parcel): Job {
            return Job(parcel)
        }

        override fun newArray(size: Int): Array<Job?> {
            return arrayOfNulls(size)
        }
    }
}