package fr.axileo.axileoapplication.features.cra.presenters

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.OnLifecycleEvent
import android.content.Context
import android.util.Log
import fr.axileo.axileoapplication.features.cra.entities.Cra
import fr.axileo.axileoapplication.features.cra.entities.Hollidays
import fr.axileo.axileoapplication.features.cra.usecases.CraUseCase
import fr.axileo.axileoapplication.launchers.AxileoApplication
import io.reactivex.CompletableObserver
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import retrofit2.HttpException
import java.net.HttpURLConnection
import javax.inject.Inject

/**
 * Created by BenChaabane on 29/01/2018.
 */
class CraPresenter(var context: Context) : CraContract.Presenter, LifecycleObserver {

    @Inject lateinit var craUseCase: CraUseCase
    lateinit var view : CraContract.View
    val compositeDisposable = CompositeDisposable()

    init {
        AxileoApplication.getApplication(context).getPresentersComponent().resolveDependencies(this)
    }
    override fun attachPresenter(view: CraContract.View, lifecycle: Lifecycle) {
        this.view = view
        lifecycle.addObserver(this)
    }

    override fun addCra(token: String, cra: Cra) {
        craUseCase.addCra(token, cra)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : CompletableObserver {
                    override fun onComplete() {
                        Log.d("CRA SUCCESS #####", "SUCCESS")
                        view.onCraAdded()
                    }

                    override fun onSubscribe(d: Disposable) {
                        if (!d.isDisposed) compositeDisposable.add(d)
                    }

                    override fun onError(e: Throwable) {
                        Log.e("CRA ERROR #####", e.message, e)
                        if (e is HttpException) {
                            when (e.code()) {
                                HttpURLConnection.HTTP_UNAUTHORIZED -> { view.onTokenInvalid() }
                                else -> view.onCraFailure(e)
                            }
                        } else view.onCraFailure(e)
                    }

                })
    }

    override fun getHolidays(token: String, year: String) {
        craUseCase.getHolidays(token, year)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : SingleObserver<Hollidays> {
                    override fun onSuccess(t: Hollidays) {
                        Log.d("Holiday SUCCESS #####", "SUCCESS")
                        view.onHolidaysRetrieved(t)
                    }

                    override fun onError(e: Throwable) {
                        Log.e("Holiday ERROR #####", e.message, e)
                        if (e is HttpException) {
                            when (e.code()) {
                                HttpURLConnection.HTTP_UNAUTHORIZED -> { view.onHolidayTokenInvalid() }
                                else -> view.onHolidaysFailure(e)
                            }
                        } else view.onHolidaysFailure(e)
                    }

                    override fun onSubscribe(d: Disposable) {
                        if (!d.isDisposed) compositeDisposable.add(d)
                    }

                })
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun detachPresenter() {
        compositeDisposable.clear()
    }
}