package fr.axileo.axileoapplication.features.clients.presenters

import android.arch.lifecycle.Lifecycle
import fr.axileo.axileoapplication.features.clients.entities.Client

/**
 * Created by BenChaabane on 22/01/2018.
 */
interface ClientsContract {

    interface Presenter {
        fun attachPresenter(view: ClientsContract.View, lifecycle: Lifecycle)
        fun getClients()
    }

    interface View {
        fun onClientsRetrieved(clients: ArrayList<Client>)
        fun onClientsError(throwable: Throwable)
    }
}