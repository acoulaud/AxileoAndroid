package fr.axileo.axileoapplication.features.events.presenters

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.OnLifecycleEvent
import android.content.Context
import android.support.annotation.NonNull
import fr.axileo.axileoapplication.features.events.entities.Event
import fr.axileo.axileoapplication.features.events.usecases.EventsUseCase
import fr.axileo.axileoapplication.launchers.AxileoApplication
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import javax.inject.Inject

/**
 * Created by BenChaabane on 15/01/2018.
 */

class EventsPresenter : EventsContract.Presenter, LifecycleObserver {

    @Inject lateinit var eventsUseCase: EventsUseCase

    lateinit var view: EventsContract.View
    var compositeDisposable : CompositeDisposable

    constructor(@NonNull context: Context) {
        AxileoApplication.getApplication(context).getPresentersComponent().resolveDependencies(this)
        compositeDisposable = CompositeDisposable()
    }

    override fun attachPresenter(view: EventsContract.View, lifeCycle: Lifecycle) {
        this.view = view
        lifeCycle.addObserver(this)
    }

    override fun getEvents() {
        eventsUseCase.getEvents()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : SingleObserver<ArrayList<Event>> {
                    override fun onSuccess(t: ArrayList<Event>) {
                        view.onEventsRetrieved(t)
                    }

                    override fun onError(e: Throwable) {
                        view.onEventsError(e)
                    }

                    override fun onSubscribe(d: Disposable) {
                        if (!d.isDisposed) compositeDisposable.add(d)
                    }
                })
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun detachPresenter() = compositeDisposable.clear()
}