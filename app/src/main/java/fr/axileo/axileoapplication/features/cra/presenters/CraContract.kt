package fr.axileo.axileoapplication.features.cra.presenters

import android.arch.lifecycle.Lifecycle
import fr.axileo.axileoapplication.features.cra.entities.Cra
import fr.axileo.axileoapplication.features.cra.entities.Hollidays

/**
 * Created by BenChaabane on 29/01/2018.
 */
interface CraContract {
    interface Presenter {
        fun attachPresenter(view : View, lifecycle: Lifecycle)
        fun addCra(token: String, cra: Cra)
        fun getHolidays(token: String, year: String)
    }
    interface View {
        fun onCraAdded()
        fun onTokenInvalid()
        fun onCraFailure(throwable: Throwable)
        fun onHolidaysRetrieved(holidays: Hollidays)
        fun onHolidayTokenInvalid()
        fun onHolidaysFailure(throwable: Throwable)
    }
}