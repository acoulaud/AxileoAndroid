package fr.axileo.axileoapplication.features.login.repositories

import fr.axileo.axileoapplication.features.authentication.constants.AuthenticationConstants.Companion.HEADER_AUTHORIZATION
import fr.axileo.axileoapplication.features.authentication.constants.AuthenticationConstants.Companion.HEADER_CONTENT_TYPE
import fr.axileo.axileoapplication.features.login.entities.Account
import fr.axileo.axileoapplication.features.login.entities.User
import io.reactivex.Completable
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.POST

/**
 * Created by BenChaabane on 25/01/2018.
 */
interface LoginRestRepository {

    @Headers(HEADER_CONTENT_TYPE)
    @POST("API/V1/AXILEO/USERS/AUTHENTIFICATION")
    fun login(@Header(HEADER_AUTHORIZATION) authorization: String,
              @Body user: User) : Single<Account>

    @Headers(HEADER_CONTENT_TYPE)
    @POST("API/V1/AXILEO/USERS/GET")
    fun changePassword(@Header(HEADER_AUTHORIZATION) authorization: String,
              @Body user: User) : Completable
}