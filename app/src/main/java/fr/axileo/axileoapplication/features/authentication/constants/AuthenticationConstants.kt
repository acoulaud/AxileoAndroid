package fr.axileo.axileoapplication.features.authentication.constants

/**
 * Created by BenChaabane on 25/01/2018.
 */
class AuthenticationConstants {

    companion object {
        val GRANT_TYPE = "password"
        val USER_NAME = "apimobile"
        val USER_PASSWORD = "apimobile"
        val CLIENT_ID = "1_3bcbxd9e24g0gk4swg0kwgcwg4o8k8g4g888kwc44gcc0gwwk4"
        val CLIENT_SECRET = "4ok2x70rlfokc8g0wws8c8kwcokw80k44sg48goc0ok4w0so0k"

        val HEADER_BEARER = "Bearer "
        const val HEADER_AUTHORIZATION = "Authorization"
        const val HEADER_CONTENT_TYPE = "Content-Type: application/json"
    }
}