package fr.axileo.axileoapplication.features.cra.repositories

import fr.axileo.axileoapplication.features.authentication.constants.AuthenticationConstants.Companion.HEADER_AUTHORIZATION
import fr.axileo.axileoapplication.features.authentication.constants.AuthenticationConstants.Companion.HEADER_CONTENT_TYPE
import fr.axileo.axileoapplication.features.cra.entities.Cra
import fr.axileo.axileoapplication.features.cra.entities.CraItem
import fr.axileo.axileoapplication.features.cra.entities.Hollidays
import fr.axileo.axileoapplication.features.login.entities.User
import fr.axileo.axileoapplication.features.login.entities.UserInfo
import io.reactivex.Completable
import io.reactivex.Single
import retrofit2.http.*

/**
 * Created by BenChaabane on 29/01/2018.
 */
interface CraRestRepository {

    @Headers(HEADER_CONTENT_TYPE)
    @POST("API/V1/AXILEO/RA/ajouter")
    fun addCra(@Header(HEADER_AUTHORIZATION) authorization: String,
               @Body cra: Cra) : Completable


    @GET("API/V1/AXILEO/EDITIONS/DAYOFF_{year}")
    fun getHolidays(@Header(HEADER_AUTHORIZATION) authorization: String,
               @Path("year") year: String) : Single<Hollidays>


    @Headers(HEADER_CONTENT_TYPE)
    @POST("API/V1/AXILEO/RA/visualiser")
    fun retrieveAllCra(@Header(HEADER_AUTHORIZATION) authorization: String,
               @Body user: User) : Single<ArrayList<CraItem>>

    @Headers(HEADER_CONTENT_TYPE)
    @POST("API/V1/AXILEO/RA/supprimer_{cra}")
    fun deleteCra(@Header(HEADER_AUTHORIZATION) authorization: String,
                  @Path("cra") craId: String,
                  @Body user: User) : Completable


}