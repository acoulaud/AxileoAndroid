package fr.axileo.axileoapplication.features.cra.ui.dialogs

import android.content.Context
import android.os.Bundle
import android.support.design.widget.TextInputEditText
import android.support.v7.app.AlertDialog
import android.support.v7.widget.AppCompatCheckBox
import android.support.v7.widget.AppCompatRadioButton
import android.view.View
import android.widget.Button
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import fr.axileo.axileoapplication.R
import fr.axileo.axileoapplication.events.AbsenceMotifEvent
import fr.axileo.axileoapplication.features.cra.entities.CraDay
import fr.axileo.axileoapplication.launchers.AxileoApplication
import org.greenrobot.eventbus.EventBus
import javax.inject.Inject

/**
 * Created by BenChaabane on 29/01/2018.
 */
class AbsenceDialog(context: Context, var craDay: CraDay) : AlertDialog(context) {

    @BindView(R.id.absence_cp) lateinit var cpCheckbox: AppCompatRadioButton
    @BindView(R.id.absence_jrtt) lateinit var jrttCheckbox: AppCompatRadioButton
    @BindView(R.id.absence_ce) lateinit var ceCheckbox: AppCompatRadioButton
    @BindView(R.id.absence_am) lateinit var amCheckbox: AppCompatRadioButton
    @BindView(R.id.absence_other) lateinit var otherCheckbox: AppCompatRadioButton
    @BindView(R.id.absence_motif) lateinit var motifEditText: TextInputEditText
    @BindView(R.id.absence_validate_btn) lateinit var validateBtn: Button
    @BindView(R.id.absence_error) lateinit var errorTxt: TextView

    @Inject lateinit var eventBus: EventBus

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AxileoApplication.getApplication(context).getDialogComponent().resolveDependencies(this)
        setContentView(R.layout.dialog_absence_choice)
        ButterKnife.bind(this)
        setCancelable(false)

        validateBtn.setOnClickListener { if (validateAbsenceMotif()) {processAbsence()} }
    }

    fun validateAbsenceMotif() : Boolean {
        if (otherCheckbox.isChecked && motifEditText.editableText.isEmpty()) {
            errorTxt.text = context.getString(R.string.absence_motif_error)
            errorTxt.visibility = View.VISIBLE
            return false
        } else if (!cpCheckbox.isChecked && !jrttCheckbox.isChecked && !ceCheckbox.isChecked &&
                !amCheckbox.isChecked && !otherCheckbox.isChecked) {
            errorTxt.text = context.getString(R.string.absence_motif_error_2)
            errorTxt.visibility = View.VISIBLE
            return false
        }
        return true
    }

    fun processAbsence() {
        val updatedCraDay = CraDay(craDay.numDay, "0",
                siteClient = false,
                cp = if (cpCheckbox.isChecked) "1" else "0",
                jrtt = if (jrttCheckbox.isChecked) "1" else "0",
                ce = if (ceCheckbox.isChecked) "1" else "0",
                am = if (amCheckbox.isChecked) "1" else "0",
                otherAbsence = if (otherCheckbox.isChecked) "1" else "0",
                otherabsenceArg = motifEditText.editableText.toString())
        eventBus.post(AbsenceMotifEvent(updatedCraDay))
        dismiss()
    }
}