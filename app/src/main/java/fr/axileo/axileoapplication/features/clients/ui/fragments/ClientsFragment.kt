package fr.axileo.axileoapplication.features.clients.ui.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.BindView
import butterknife.ButterKnife
import com.rbrooks.indefinitepagerindicator.IndefinitePagerIndicator
import fr.axileo.axileoapplication.R
import fr.axileo.axileoapplication.events.UpdateToolbarTitleEvent
import fr.axileo.axileoapplication.features.clients.adapters.ClientsAdapter
import fr.axileo.axileoapplication.features.clients.entities.Client
import fr.axileo.axileoapplication.features.clients.presenters.ClientsContract
import fr.axileo.axileoapplication.features.clients.presenters.ClientsPresenter
import fr.axileo.axileoapplication.launchers.AxileoApplication
import kotlinx.android.synthetic.main.fragment_clients.*
import org.greenrobot.eventbus.EventBus
import javax.inject.Inject

/**
 * Created by BenChaabane on 22/01/2018.
 */
class ClientsFragment : Fragment(), ClientsContract.View {
    @BindView(R.id.clients_recyclerView) lateinit var clientsRecyclerView: RecyclerView
    @BindView(R.id.recyclerView_pager_indicator) lateinit var pagerIndicator: IndefinitePagerIndicator

    @Inject lateinit var eventBus: EventBus

    lateinit var clients: ArrayList<Client>
    lateinit var clientsAdapter: ClientsAdapter
    lateinit var presenter: ClientsPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AxileoApplication.getApplication(context).getFragmentsComponent().resolveDependencies(this)
        clients = savedInstanceState?.getParcelableArrayList(CLIENTS_BUNDLE) ?: ArrayList()
        clientsAdapter = ClientsAdapter(clients)
        presenter = ClientsPresenter(context)
        presenter.attachPresenter(this, lifecycle)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_clients, container, false)
        ButterKnife.bind(this, view)
        with(clientsRecyclerView) {
            hasFixedSize()
            adapter = clientsAdapter
        }
        pagerIndicator.attachToRecyclerView(clientsRecyclerView)
        if (clients.size == 0) {
            presenter.getClients()
        }
        return view
    }

    override fun onResume() {
        super.onResume()
        eventBus.post(UpdateToolbarTitleEvent(getString(R.string.clients)))
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putParcelableArrayList(CLIENTS_BUNDLE, clients)
    }


    override fun onClientsRetrieved(clients: ArrayList<Client>) {
        clientsAdapter.updateData(clients)
    }

    override fun onClientsError(throwable: Throwable) {
        if (isAdded) {
            AlertDialog.Builder(context)
                    .setMessage(R.string.splash_warning_text)
                    .setOnDismissListener({activity.supportFragmentManager.popBackStackImmediate()})
                    .create()
                    .show()
        }
    }

    companion object {
        val TAG = ClientsFragment::class.java.simpleName
        val CLIENTS_BUNDLE = TAG + ":CLIENTS_BUNDLE"
        fun newInstance() = ClientsFragment()
    }
}