package fr.axileo.axileoapplication.features.events.repositories

import fr.axileo.axileoapplication.features.events.entities.Event
import io.reactivex.Single
import io.reactivex.annotations.NonNull
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


/**
 * Created by BenChaabane on 15/01/2018.
 */

@Singleton
class EventsRepositoryImpl : EventsRepository {

    var service: EventsRestRepository

    constructor(@NonNull retrofit: Retrofit) {
        service = retrofit.create(EventsRestRepository::class.java)
    }

    override fun getEvents(): Single<ArrayList<Event>> {
        return service.getEvents("").subscribeOn(Schedulers.io())
                .timeout(1, TimeUnit.MINUTES)
    }


}