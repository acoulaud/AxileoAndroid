package fr.axileo.axileoapplication.features.cra.ui.fragments

import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import fr.axileo.axileoapplication.R
import fr.axileo.axileoapplication.constants.SharedPreferencesConstants
import fr.axileo.axileoapplication.events.CreateCraEvent
import fr.axileo.axileoapplication.events.UpdateToolbarTitleEvent
import fr.axileo.axileoapplication.features.authentication.constants.AuthenticationConstants
import fr.axileo.axileoapplication.features.authentication.entities.OauthToken
import fr.axileo.axileoapplication.features.authentication.presenters.AuthenticateContract
import fr.axileo.axileoapplication.features.authentication.presenters.AuthenticatePresenter
import fr.axileo.axileoapplication.features.cra.adapters.CraListAdapter
import fr.axileo.axileoapplication.features.cra.entities.CraItem
import fr.axileo.axileoapplication.features.cra.presenters.CraListContract
import fr.axileo.axileoapplication.features.cra.presenters.CraListPresenter
import fr.axileo.axileoapplication.features.login.entities.User
import fr.axileo.axileoapplication.features.login.entities.UserInfo
import fr.axileo.axileoapplication.launchers.AxileoApplication
import org.greenrobot.eventbus.EventBus
import java.util.*
import javax.inject.Inject


/**
 * Created by BenChaabane on 30/01/2018.
 */
class CraListFragment : Fragment(), CraListContract.View, AuthenticateContract.View, CraListAdapter.Listener {
    @BindView(R.id.cra_recycler) lateinit var craRecycler: RecyclerView
    @BindView(R.id.cra_empty_list_txt) lateinit var emptyCraTxt: TextView
    @BindView(R.id.cra_creation_btn) lateinit var createCraBtn: Button
    @BindView(R.id.cra_progressBar) lateinit var progress: ProgressBar

    lateinit var craAdapter: CraListAdapter
    lateinit var craItems: ArrayList<CraItem>

    lateinit var craListPresenter: CraListPresenter
    lateinit var authenticatePresenter: AuthenticatePresenter

    lateinit var token: String
    lateinit var login: String
    lateinit var password: String

    @Inject lateinit var sharedPrefs: SharedPreferences
    @Inject lateinit var eventBus: EventBus

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AxileoApplication.getApplication(context).getFragmentsComponent().resolveDependencies(this)
        craItems = savedInstanceState?.getParcelableArrayList(BUNDLE_CRA_ITEMS) ?: ArrayList()
        craAdapter = CraListAdapter(craItems)
        craAdapter.setListener(this)

        craListPresenter = CraListPresenter(context)
        craListPresenter.attachPresenter(this, lifecycle)

        authenticatePresenter = AuthenticatePresenter(context)
        authenticatePresenter.attachPresenter(this, lifecycle)

        login = sharedPrefs.getString(SharedPreferencesConstants.USER_LOGIN, "")
        password = sharedPrefs.getString(SharedPreferencesConstants.USER_PASSWORD, "")
        token = sharedPrefs.getString(SharedPreferencesConstants.TOKEN_SHARED_PREFERENCE, "")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_cra_list, container, false)
        ButterKnife.bind(this, view)
        with (craRecycler) {
            hasFixedSize()
            adapter = craAdapter
        }
        createCraBtn.setOnClickListener({
            if (craItems.isEmpty() || !isCraAlreadyCreated()) {
                eventBus.post(CreateCraEvent())
            } else {
                if (isAdded) {
                    AlertDialog.Builder(activity)
                            .setMessage(R.string.cra_already_exist)
                            .setPositiveButton(android.R.string.ok) { dialog, which -> dialog.dismiss()}
                            .create()
                            .show()
                }
            }
        })
        return view
    }

    override fun onResume() {
        super.onResume()
        eventBus.post(UpdateToolbarTitleEvent(getString(R.string.cra)))
        val userInfo = UserInfo(login, password)
        val user = User(userInfo)
        craListPresenter.retrieveAllCra("${AuthenticationConstants.HEADER_BEARER}$token",
                user)
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putParcelableArrayList(BUNDLE_CRA_ITEMS, craItems)
    }

    override fun onAllCraRetrieved(craList: ArrayList<CraItem>) {
        progress.visibility = View.GONE
        craItems = craList
        if (craList.isEmpty()) {
            emptyCraTxt.visibility = View.VISIBLE
            craRecycler.visibility = View.GONE
        } else {
            emptyCraTxt.visibility = View.GONE
            craRecycler.visibility = View.VISIBLE
            craAdapter.updateData(craList)
        }

    }

    override fun onTokenInvalid() {
        authenticatePresenter.authenticate()
    }

    override fun onAllCraFailure(throwable: Throwable) {
        progress.visibility = View.GONE
        if (isAdded) {
            AlertDialog.Builder(activity)
                    .setMessage(R.string.splash_login_warning_text)
                    .setPositiveButton(android.R.string.ok) { dialog, which -> dialog.dismiss()}
                    .create()
                    .show()
        }
    }

    override fun onAuthenticationSuccess(oauthToken: OauthToken) {
        sharedPrefs.edit().putString(SharedPreferencesConstants.TOKEN_SHARED_PREFERENCE, oauthToken.accessToken).apply()
        token = oauthToken.accessToken
        val userInfo = UserInfo(login, password)
        val user = User(userInfo)
        craListPresenter.retrieveAllCra("${AuthenticationConstants.HEADER_BEARER}$token",
                user)

    }

    override fun onAuthenticationFailed(throwable: Throwable) {
        progress.visibility = View.GONE
        if (isAdded) {
            AlertDialog.Builder(activity)
                    .setMessage(R.string.splash_login_warning_text)
                    .setPositiveButton(android.R.string.ok) { dialog, which -> dialog.dismiss()}
                    .create()
                    .show()
        }
    }

    override fun onCraDeleted() {
        progress.visibility = View.GONE
        if (isAdded) {
            AlertDialog.Builder(activity)
                    .setMessage(R.string.cra_delete_success)
                    .setPositiveButton(android.R.string.ok) { dialog, which -> dialog.dismiss()}
                    .create()
                    .show()
        }
        val userInfo = UserInfo(login, password)
        val user = User(userInfo)
        craListPresenter.retrieveAllCra("${AuthenticationConstants.HEADER_BEARER}$token",
                user)
    }

    override fun onCraDeleteFailure(throwable: Throwable) {
        progress.visibility = View.GONE
        if (isAdded) {
            AlertDialog.Builder(activity)
                    .setMessage(R.string.cra_delete_failure)
                    .setPositiveButton(android.R.string.ok) { dialog, which -> dialog.dismiss()}
                    .create()
                    .show()
        }
    }

    override fun onDeleteCra(craItem: CraItem) {
        progress.visibility = View.VISIBLE
        val userInfo = UserInfo(login, password)
        val user = User(userInfo)
        craListPresenter.deleteCra("${AuthenticationConstants.HEADER_BEARER}$token",
                "${craItem.id}", user)
    }


    fun isCraAlreadyCreated() : Boolean {
        val calendar = Calendar.getInstance()
        val currentMonth = "${calendar.get(Calendar.MONTH) + 1}"
        val currentYear = "${calendar.get(Calendar.YEAR)}"
        return craItems.any { currentMonth.equals(it.month) && currentYear.equals(it.year) }
    }

    companion object {
        val TAG = CraListFragment::class.java.simpleName
        val BUNDLE_CRA_ITEMS = TAG + ":BUNDLE_CRA_ITEMS"
        fun newInstance() = CraListFragment()
    }
}