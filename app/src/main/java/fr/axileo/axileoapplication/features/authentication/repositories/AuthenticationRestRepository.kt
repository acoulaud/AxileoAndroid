package fr.axileo.axileoapplication.features.authentication.repositories

import fr.axileo.axileoapplication.features.authentication.entities.OauthToken
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by BenChaabane on 25/01/2018.
 */
interface AuthenticationRestRepository{

    @GET("oauth/v2/token")
    fun authenticate(@Query("grant_type") grantType: String,
                     @Query("client_id") clientId: String,
                     @Query("client_secret") clientSecret: String,
                     @Query("username") username: String,
                     @Query("password") password: String) : Single<OauthToken>
}