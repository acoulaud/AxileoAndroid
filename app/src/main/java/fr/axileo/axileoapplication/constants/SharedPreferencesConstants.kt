package fr.axileo.axileoapplication.constants

/**
 * Created by BenChaabane on 25/01/2018.
 */
class SharedPreferencesConstants {

    companion object {
        val TOKEN_SHARED_PREFERENCE = "token_shared_preference"
        val USER_LOGIN = "user_login"
        val USER_PASSWORD = "user_password"
    }
}