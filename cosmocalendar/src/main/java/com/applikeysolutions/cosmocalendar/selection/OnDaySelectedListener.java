package com.applikeysolutions.cosmocalendar.selection;

import com.applikeysolutions.cosmocalendar.model.Day;

public interface OnDaySelectedListener {
    void onDaySelected(Day day, boolean isRemove);
}
